#encoding: utf-8
from __future__                              import unicode_literals
from django.conf                             import settings
from django.contrib                          import messages
from django.contrib.auth                     import (authenticate, login as user_login, logout as user_logout,
                                                     update_session_auth_hash)
from django.contrib.auth.models              import User
from django.contrib.auth.decorators          import login_required
from django.views.decorators.cache           import never_cache
from django.contrib.auth.password_validation import validate_password
from django.core                             import serializers
from django.core.exceptions                  import ObjectDoesNotExist, ValidationError
from django.core.validators                  import validate_email
from django.core.paginator                   import Paginator, EmptyPage, PageNotAnInteger
from django.core.files                       import File
from django.core.files.storage               import default_storage
from django.core.mail                        import send_mail, EmailMessage
from django.template.loader                  import render_to_string
from django.db.models                        import Max, Q
from django.shortcuts                        import render, redirect, get_object_or_404, Http404
from django.http                             import JsonResponse, HttpResponse
from datetime                                import datetime, timedelta, date
from django.utils                            import timezone
#from dateutil.relativedelta                  import relativedelta
from math                                    import floor
from DHSite.funcoes                          import *
from DHSite.models                           import *
import calendar
import json
import pytz
import time
import os


# Constantes
tz = pytz.timezone("America/Fortaleza")
mesListaStr = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
               'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']


#PÁGINA INICIAL #############################################################################################################

def index(request):
    
    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Conheça", paginaTitulo="")

    # us = usuario.objects.get(id=7)
    # us.permissoes = None
    # us.save()

    return render(request, 'index.html', vars())



#LOGIN ######################################################################################################################

def login(request):

    # Negando a entrada do usuário já logado
    if request.user.is_authenticated:
        return redirect('/painel/')

    # Definições da página
    pg = pagina(request, abaTitulo="DoHere | Entrar")

    if request.method == 'POST':

        user = authenticate(username=request.POST['email'], password=request.POST['password'])

        if user is not None:
            if user.is_active:
                if user.last_login is None:
                    user_login(request, user)
                    return redirect('/usuarioDados/primeiroAcesso/')
                else:
                    user_login(request, user)
                    return redirect('/painel/')
            else:
                messages.error(request, 'Esta conta de usuário está desativada.')
        else:
            messages.error(request, 'E-mail ou senha inválida.')

    return render(request, 'login.html', vars())


# LOGOUT ####################################################################################################################

@login_required
def logout(request):
    user_logout(request)
    return redirect('/')


#PAINEL DO USUÁRIO ##########################################################################################################

@login_required
def painel(request):

    # Permissões do usuário
    usPermissoes     = carregPermissoes(request.user.usuario)
    # Opções do Painel
    painelOpcaoLista = carregPainelOpcoes(usPermissoes, permissao, painelOpcao)

    # Definições da página
    pg   = pagina(request, abaTitulo="DoHere | Painel", paginaTitulo="PAINEL")
    tema = request.user.usuario.tipo.painelTema.arquivoCss

    # Gerando o número de pedidos em análise no painelOpcao "Pedidos de Reservas"
    #Funciona apenas para o painelOpcao "Pedidos de Reserva"
    #No futuro pode haver uma implementação que se adapta pra todos os painelOpcao's
    for painelOpc in painelOpcaoLista:
        if painelOpc.exibeNotifNumero:
            painelOpc.notifQuant = pedido.objects.filter(status__id=3,
                                                         usuario__campus=request.user.usuario.campus)\
                                                 .count() #Pedidos não analisados no campus do solicitante

    return render(request, 'painel.html', vars())


# CADASTRO DE USUÁRIOS  ##################################################################################################
# Seleção dos tipos de usuário
@login_required
def usuarioTipoView(request):

    # Verificando as permissões do usuário
    if not verifPermissoes(request, permissao, [9]): # ID=9 Cadastrar Usuário Local
        raise Http404()

    # Definições da página
    pg = pagina(request, abaTitulo="DoHere | Cadastro", paginaTitulo="CADASTRO DE USUÁRIO")

    #excludeId: Ids que serão excluídos da busca no banco
    excludeId = [1] if verifPermissoes(request, permissao, [7]) else [1,2] # ID=7 Cadastrar Administrador Local

    if request.user.usuario.tipo.id == 1: #SuperAdmin
        excludeId = [] #Nenhum tipo será excluído

    # Tipos de usuários para cadastro
    usTipos = usuarioTipo.objects.filter(ativo=True)\
                                 .exclude(id__in=excludeId)\
                                 .order_by('id')

    return render(request, 'usuario/cadastro/selecionaTipo.html', vars())


# Página de Cadastro de usuário
@login_required
def usuarioCadastro(request, usuarioTipoId, acao):

    if not verifPermissoes(request, permissao, [9]): #ID=9 Cadastrar Usuário Local
        return redirect("/painel/")

    # Verificando se o Id do tipo de usuário informado existe no banco e está ativo
    try:
        usTipo = usuarioTipo.objects.get(id=usuarioTipoId, ativo=True)
    except usuarioTipo.DoesNotExist:
        messages.error(request, "Selecione um tipo de usuário válido!")
        return redirect('/usuarioCadastro/tipo/')

    # Definições da Página
    pg = pagina(request, abaTitulo="DoHere | Cadastro", paginaTitulo="CADASTRO DE USUÁRIO")

    #  Variáveis usadas ao entrar no formulário pela primeira vez quanto ou quando há algum erro na ação "salvar"
    # Campos adicionais do formulário de cadastro. Alguns campos mudam de acordo com o tipo de usuário
    inputLista = usTipo.cadastroInputLista.split(",")
    # Setando os dropdowns para serem exibidos na tela de cadastro, caso o tipo de usuário selecionado necessite
    cursos  = curso.objects.filter(campus=request.user.usuario.campus, ativo=True).order_by('nome')
    setores = setor.objects.filter(campus=request.user.usuario.campus, ativo=True).order_by('nome')
    campusLista = campus.objects.filter(ativo=True)


    # Entrada via submissão do formulário preenchido, ao clicar em "Cadastrar"
    if acao == 'salvar':

        # Verificando se os dados de cadastro foram fornecidos pelo usuário
        if 'email'    in request.POST and\
           'nome'     in request.POST:

            email    = request.POST['email']
            nome     = request.POST['nome']
        else:
            messages.error(request, 'Houve um erro! Algum dado não foi informado.')
            return render(request, 'usuario/cadastro/formulario.html/', vars())

        # Verificando a existência dos dados adicionais. Esses dados são fornecidos de acordo com o tipo de usuário
        # Caso não exista, a variável correspondente é setada com None para que seja pulada a etapa de conferência e cadastro
        cpf       = None if 'cpf' not in request.POST else request.POST['cpf']
        cnpj      = None if 'cnpj' not in request.POST else request.POST['cnpj']
        telefone  = None if 'telefone' not in request.POST else request.POST['telefone']
        matricula = None if 'matricula' not in request.POST else request.POST['matricula']
        cursoId   = None if 'cursoId' not in request.POST else request.POST['cursoId']
        setorId   = None if 'setorId' not in request.POST else request.POST['setorId']
        campusId  = None if 'campusId' not in request.POST else request.POST['campusId']

        #  VALIDAÇÕES -------------------------------------------------------------------------------------
        # Validação do E-mail -----------------------------------------------------------------------------
        #Situação 1: E-mail fora do padrão. Inválido
        try:
            validate_email(email)
        except validate_email.ValidationError:
            messages.error(request, 'O E-mail '+email+' não é válido!')
            return render(request, 'usuario/cadastro/formulario.html/', vars())

        #Situação 2: E-mail já cadastrado
        if email in User.objects.values_list('email', flat=True):
            messages.error(request, 'O E-mail '+email+' já está em uso!\
                                       Entre em contato com o Suporte para mais informações.')
            return render(request, 'usuario/cadastro/formulario.html/', vars())


        # Validação da Senha -----------------------------------------------------------------------------
        # #Situação 1: Senhas diferentes
        # if not verif_senha_dupla(senha, senhaRep):
        #     messages.error(request, "As senhas digitadas estão diferentes! Tente novamente.")
        #     return render(request, 'usuario/cadastro/formulario.html/', vars())

        # #Situação 2: Senha Fraca
        # try:
        #     validate_password(senha)
        # except ValidationError:
        #     messages.error(request, "Sua senha deve ter no mínimo 8 dígitos e conter letras e números.")
        #     return render(request, 'usuario/cadastro/formulario.html/', vars())


        #Validação do Nome -----------------------------------------------------------------------------------
        # Validação superficial. Fazer de uma forma mais completa
        if len(nome) < 5:
            messages.error(request, "O nome \""+nome+"\" não é válido! Informe seu Nome Completo.")
            return render(request, 'usuario/cadastro/formulario.html/', vars())


        #Validação do CPF -------------------------------------------------------------------------------------
        if cpf is not None:
            #!!! remover os não dígitos do cpf
            #Situação 1: O número do CPF já está cadastrado no sistema
            if cpf in usuario.objects.values_list('cpf', flat=True):
                messages.error(request, "Já existe um usuário com esse "+cpf+"!")
                return render(request, 'usuario/cadastro/formulario.html/', vars())

            #Situação 2: CPF inválido!
            if not validar_cpf(cpf):
                messages.error(request, 'CPF Inválido! Verifique os números digitados')
                return render(request, 'usuario/cadastro/formulario.html/', vars())


        #Validação do CNPJ --------------------------------------------------------------------------------
        if cnpj is not None:
            #!!! Remover todos os não dígitos de cnpj
            #Situação 1: CNPJ já está cadastrado
            if cnpj in usuario.objects.values_list('cnpj', flat=True):
                messages.error(request, "O CNPJ "+cnpj+" já está em uso!")
                return render(request, 'usuario/cadastro/formulario.html/', vars())

            #Situação 2: CNPJ inválido
            if not validar_cnpj(request.POST['cnpj']):
                messages.error(request, 'CNPJ Inválido!')
                return render(request, 'usuario/cadastro/formulario.html/', vars())


        #Validação da Matrícula ---------------------------------------------------------------------------
        if matricula is not None:
            #Situação 1: O número de matrícula já está em uso
            if matricula in usuario.objects.values_list('matricula', flat=True):
                messages.error(request, "A Matrícula "+matricula+" já está em uso!")
                return render(request, 'usuario/cadastro/formulario.html/', vars())

            #Situação 2: Matrícula Inválida
            if len(matricula) != 7: #ATENÇÃO! Criar uma função
                messages.error(request, "Matrícula Inválida!")
                return render(request, 'usuario/cadastro/formulario.html/', vars())


        #Validação do Curso --------------------------------------------------------------------------------
        if cursoId is not None:
            #Caso raro em que o curso é desativado enquanto está sendo feito o cadastro do usuário
            #Ou uma tentativa de burlar o sistema tentando enviar um cursoId diferente dos disponíveis
            try:
                cursoObj = curso.objects.get(id=cursoId, ativo=True)
            except curso.DoesNotExist:
                messages.error(request, "O Curso escolhido está desativado ou não existe em nosso banco! \
                                         Entre em contato com o Suporte para mais informações.")
                return render(request, 'usuario/cadastro/formulario.html/', vars())


        #Validação do Setor ---------------------------------------------------------------------------------
        if setorId is not None:
            #Caso raro em que o setor é desativado enquanto está sendo feito o cadastro do usuário
            #Ou uma tentativa de burlar o sistema tentando enviar um setorId diferente dos disponíveis
            try:
                setorObj = setor.objects.get(id=setorId, ativo=True)
            except setor.DoesNotExist:
                messages.error(request, "O setor submetido está desativado ou não exite em nosso banco! \
                                         Entre em contato com o Suporte para mais informações.")
                return render(request, 'usuario/cadastro/formulario.html/', vars())


        #Validação do Telefone ------------------------------------------------------------------------------
        # if telefone is not None:
        #     #Validação provisória
        #     if not validar_telefone(telefone):
        #         messages.error(request, 'O telefone '+telefone+' é inválido.')
        #         return render(request, 'usuario/cadastro/formulario.html/', vars())


        #Validação do Campus ---------------------------------------------------------------------------------
        if campusId is not None:
            #Caso raro em que o campus é desativado enquanto está sendo feito o cadastro do usuário
            #Ou uma tentativa de burlar o sistema tentando enviar um setorId diferente dos disponíveis
            try:
                campusObj = campus.objects.get(id=campusId, ativo=True)
            except campus.DoesNotExist:
                messages.error(request, "O campus submetido está desativado ou não exite em nosso banco! \
                                         Entre em contato com o Suporte para mais informações.")
                return render(request, 'usuario/cadastro/formulario.html/', vars())


        #   FIM DAS VALIDAÇÕES ----------------------------------------------------------------------------------


        #   PREPARANDO PARA SALVAR NO BANCO DE DADOS
        #O User do Django não pode ter espaços em seu "username", por isso é necessário converter os espaços em sublinhas
        #É obrigatório que este nome seja único, então devem ser passados os nomes atuais rsgistrados no BD para que seja
        #feita a checagem
        djangoUserNome = convParaDjangoUserName(nome, User.objects.values_list('username', flat=True))
        #convParaDjangoUserName() retorna None quando tem muitos usuários com o mesmo nome
        if djangoUserNome == None:
            messages.error(request, "Existem muitos usuários com o mesmo nome submetido! Contate o Suporte.")
            return render(request, 'usuario/cadastro/formulario.html/', vars())

        # Setando o campus
        #Para cadastro regular, o campus é o mesmo do usuário que está cadastrando.
        #Para cadastro especial (feito por um SuperAdmin), o campus é o que foi escolhido no formulário
        campusObj = request.user.usuario.campus if (request.user.usuario.tipo.id > 1 and campusId is None) else campusObj

        #Setando a senha padrão
        #A senha padrão é o CPF ou CNPJ do usuário
        senha = cpf if cpf is not None else cnpj
        
        if senha is None:
            messages.error(request, "CPF/CNPJ não foi informado!")
            return render(request, 'usuario/cadastro/formulario.html/', vars())


        #  SALVANDO O NOVO USUÁRIO NO BANCO DE DADOS
        #Tabela de usuário do Django
        djangoUser = User.objects.create_user(
            username = djangoUserNome,
            email    = email,
            password = senha,
        )

        #Tabela de usuários própria do DoHere
        usuarioObj = usuario(
            user   = djangoUser,
            nome   = nome,
            tipo   = usTipo,
            campus = campusObj,
        )

        #Inserindo os campos específicos de cada tipo de usuário
        usuarioObj.matricula = matricula
        usuarioObj.cpf       = cpf
        usuarioObj.cnpj      = cnpj
        usuarioObj.curso     = cursoObj if cursoId is not None else None
        usuarioObj.setor     = setorObj if setorId is not None else None
        usuarioObj.telefone  = telefone

        # Registrando o usuário no Banco de Dados
        usuarioObj.save() # Usuário foi registrado no sistema e já está Ativo

        # Salvando esta atividade no Log
        logAtiv = log(
            atividade = "NOVO USUÁRIO | ID="+str(usuarioObj.id)+" | Nome="+usuarioObj.nome,
            usuario   = request.user.usuario
        )
        logAtiv.save()

        notfic = notificacao(
            descricao        = "{"+usuarioObj.nome+"} ("+usuarioObj.tipo.nome+") foi cadastrado(a)",
            usuarioOrigem    = request.user.usuario,
            permissaoDestino = permissao.objects.get(id=6), #ID=6 Controle de Usuários
            link             = '/usuarios/exibir/'+str(usuarioObj.id)+'/',
            campus           = campusObj
        )
        notfic.save()

        #Notificando o usuário por email
        send_mail(
            subject        = 'Cadastrado com Sucesso!',
            message        = render_to_string('emails/usuarioCadastrado.txt', vars()),
            from_email     = 'nao-responda@plataformadohere.com.br',
            recipient_list = [email],
            fail_silently  = True,
        )

        messages.success(request, 'Usuário cadastrado com sucesso!')
        return redirect("/usuarios/listar/")


    # PRIMEIRA ENTRADA NESTA VIEW. RENDERIZAÇÃO O FORMULÁRIO DE CADASTRO
    return render(request, 'usuario/cadastro/formulario.html', vars())


#CALENDÁRIO #################################################################################################################
@login_required
def calendario(request, campusId):

    # Verificando as permissões do usuário    
    if not verifPermissoes(request, permissao, [28]): # ID=28 Acessar Calendário Local
        raise Http404()

    # Verificando se o usuário tem permissão para visualizar o calendário de outros campus
    altCampus = alternaCampus(request, permissao, campus, campusId)
    if altCampus is None and campusId is not None:
        return redirect('/calendario/')

    # Definições da Página
    pg = pagina(request, abaTitulo="DoHere | Calendário", paginaTitulo="CALENDÁRIO")

    # É só isso mesmo! O fullCalendar pega os dados via Ajax
    return render(request, 'calendario.html', vars())



@login_required
def reservasHoje(request, diaAtual):
    
    if not verifPermissoes(request, permissao, [30]): # ID=30 Acessar Reservas de Hoje
        raise Http404()

    # Definições da Página
    pg = pagina(request, abaTitulo="DoHere | Reservas", paginaTitulo="RESERVAS")

    hoje = tz.localize(datetime.today())

    if diaAtual == "hoje":
        diaAtual = hoje
    else:
        diaAtual = tz.localize(datetime.strptime(diaAtual, "%d-%m-%Y"))
    
    diaAnter = diaAtual - timedelta(days=1)
    diaSegui = diaAtual + timedelta(days=1)

    pedidosInicioDiaAtual = criarRcLista(recurso, pedido.objects.filter(usuario__campus=request.user.usuario.campus,
                                                                        reserva__inicio__year=diaAtual.year,
                                                                        reserva__inicio__month=diaAtual.month,
                                                                        reserva__inicio__day=diaAtual.day,
                                                                        status__id=1)\
                                                                .order_by('reserva__inicio'))

    pedidosFimDiaAtual = criarRcLista(recurso, pedido.objects.filter(usuario__campus=request.user.usuario.campus,
                                                                     reserva__fim__year=diaAtual.year,
                                                                     reserva__fim__month=diaAtual.month,
                                                                     reserva__fim__day=diaAtual.day,
                                                                     status__id=1)\
                                                             .order_by('reserva__fim'))


    return render(request, 'pedidos/reservasHoje.html', vars())



#RESERVAR ###################################################################################################################
@login_required
def reservar(request, modo, etapa):

    # Verificando as permissões do usuário
    if not verifPermissoes(request, permissao, [18]): #ID=18 Criar Pedido de Reserva
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Crie seu Pedido", paginaTitulo="CRIE SEU PEDIDO")

    # Primeira etapa
    if etapa == "selModo":
        # Expirando os pedidos com reservas já passadas
        expirarPedidos(request.user.usuario, pedido, pedidoStatus)

        # Verificando se o usuário já atingiu o limite de pedidos Ativos
        if usuarioPedLimite(request.user.usuario, pedido):
            messages.warning(request, "<strong>Seu limite de pedidos ativos foi alcançado!</strong> \
                                       Por favor, faça uso da sua reserva mais próxima ou cancele uma reserva \
                                       para poder criar um novo Pedido.")

            return redirect('/painel/')

        pg.defPaginaTitulo("ESCOLHA UM MODO DE RESERVA")
        return render(request, 'pedidos/reservarSelModo.html', vars())


    # Etapa selUnidade. Comum aos modos rp e pr
    if etapa == "selUnidade":
        # Unidades ativas do campus
        unidadeQs = unidade.objects.filter(ativo=True, campus=request.user.usuario.campus)

        if modo == "rp":
            formAction = "/reservar/" + modo + "/selRecurso/"
        elif modo == "pr":
            formAction = "/reservar/" + modo + "/selPeriodo/"

        pg.defPaginaTitulo("SELECIONE A UNIDADE DO CAMPUS")
        return render(request, 'pedidos/reservarSelUnidade.html', vars())

    # Modo Recurso-Período (rp)
    # Primeiro se escolhe o recurso a ser reservado e depois é exibido o calendário onde são mostrados os pedidos em que o
    # recurso selecionado foi reservado. Nesse calendário o usuário escolhe o período do pedido.
    if modo == "rp":

        if etapa == "selRecurso":
            formAction = "/reservar/" + modo + "/selPeriodo/"
            unidadeObj = get_object_or_404(unidade,
                                           id=request.POST['unidadeId'],
                                           ativo=True,
                                           campus=request.user.usuario.campus)

            # Lista de Recursos disponibilizados na Unidade selecionada
            recursosLista = recurso.objects.filter(ativo=True, unidade=unidadeObj).order_by("nome")
            recursos = []

            #Adicionando na lista "recursos[]" os recursos disponíveis para o tipo do usuário solicitante
            for rc in recursosLista:
                if unicode(request.user.usuario.tipo.id) in rc.disponibUsuario.split(','):
                    recursos.append(rc)

            #Tipos de recurso para as TABS
            #rcTipoRemoveVazio verifica se há Tipos de Recurso que não estão sendo usados em "recursos"
            recursoTipoLista = rcTipoRemoveVazio(list(recursoTipo.objects.filter(ativo=True)), recursos)

            pg.defPaginaTitulo("SELECIONE O RECURSO DO SEU PEDIDO")
            return render(request, 'pedidos/reservarSelRecursosRP.html', vars())

        if etapa == "selPeriodo":
            formAction = "/reservar/" + modo + "/resumo/"

            #Deve haver ao menos 1 recurso selecionado
            if "recursoSelecionado" in request.POST:
                reservaRecursoId = request.POST["recursoSelecionado"]
            else:
                messages.error(request, "Selecione pelo menos 1 recurso da lista!")
                return redirect("/reservar/" + modo + "/selRecurso/")

            pg.defPaginaTitulo("SELECIONE O PERÍODO DA RESERVA")

            return render(request, 'pedidos/reservarSelPeriodoRP.html', vars())

    # Modo Período-Recurso
    # Primeiro se escolhe o período e depois os recursos que podem ser reservados no período selecionado
    if modo == "pr":
        if etapa == "selPeriodo":
            formAction = "/reservar/" + modo + "/selRecurso/"
            pg.defPaginaTitulo("SELECIONE O PERÍODO DA RESERVA")

            #É só isso mesmo! O fullCalendar pega os dados via Ajax
            return render(request, 'pedidos/reservarSelPeriodo.html', vars())

        elif etapa == "selRecurso":
            formAction = "/reservar/" + modo + "/resumo/"

            try:
                inicio = tz.localize(datetime.strptime(request.POST['reservaInicio'], "%Y-%m-%dT%H:%M:%S"))
                fim    = tz.localize(datetime.strptime(request.POST['reservaFim'],    "%Y-%m-%dT%H:%M:%S"))
            except ValueError:
                inicio = tz.localize(datetime.strptime(request.POST['reservaInicio'], "%Y-%m-%d"))
                fim    = tz.localize(datetime.strptime(request.POST['reservaFim'],    "%Y-%m-%d"))
            except KeyError:
                messages.error(request, "Ocorreu um erro! Tente novamente.")
                return redirect("/reservar/"+ modo +"/selUnidade/")

            #Validação do período selecionado no calendário
            #Caso o usuário tente alterar as configurações no fullcalendar.js para selecionar fora do período válido
            if inicio <= tz.localize(datetime.now()):
                messages.error(request, "O período selecionado é inválido!")
                return redirect("/reservar/"+ modo +"/selUnidade/")

            # Unidade selecionada
            unidadeObj = get_object_or_404(unidade,
                                           id=request.POST['unidadeId'],
                                           ativo=True,
                                           campus=request.user.usuario.campus)

            #Filtrando os recursos a serem mostrados para o usuário
            recursosQs = recurso.objects.filter(ativo=True, unidade=unidadeObj).order_by("nome")

            # ADIÇÃO NA LISTA "recursos[]" OS RECURSOS DISPONÍVEIS PARA O TIPO DO USUÁRIO SOLICITANTE
            # E OS RECURSOS QUE PODEM SER RESERVADOS NO PERÍODO SELECIONADO
            recursos = []

            for rc in recursosQs:
                rc.dispHoraInicio = tz.localize(datetime.strptime(str(inicio.date())+str(rc.dispHoraInicio), "%Y-%m-%d%H:%M:%S"))
                rc.dispHoraFim    = tz.localize(datetime.strptime(str(fim.date())+str(rc.dispHoraFim), "%Y-%m-%d%H:%M:%S"))
                
                if str(rc.dispHoraFim.time()) == "00:00:00":
                    rc.dispHoraFim = rc.dispHoraFim + timedelta(days=1)

                if (rc.dispHoraInicio > inicio) or (rc.dispHoraFim < fim):
                    rc.indispMotivo = 'Horário indisponível' # TEMPLATE - Recurso indisponível para o horário selecionado
                    rc.quantidade = 0

                if (unicode(request.user.usuario.tipo.id) in rc.disponibUsuario.split(',')):
                    recursos.append(rc)

            # Verificação dos recursos usados no pedidos "aceitos" e "em análise"
            # status do pedido | 1-Aceito, 3-Em Análise | pedidos criados até ano passado
            pedidos = pedido.objects.filter(status__id__in=[1,3],
                                            usuario__campus=request.user.usuario.campus,
                                            reserva__fim__gte=(datetime.today()))
                                            #dataCriacao__year__gte=(datetime.today().year-1)

            #Bloqueando recursos que estão em outros pedidos de mesmo período escolhido no calendário
            for ped in pedidos:
                #Comparação do período do pedido "ped" com o período selecionado pelo usuário
                if interseccao(inicio, fim, ped.reserva.inicio, ped.reserva.fim):
                    #Caso haja intersecção do período selecionado com o período de algum pedido já aceito ou em análise,
                    #então é feita um subtração na quantidade de cada recurso que será disponibilizado para reserva
                    for pedRc in ped.recursos.split(';'):
                        pedRc = pedRc.split(',')
                        for rc in recursos: #lista de recursos que serão disponibilizados para reserva
                            if rc.id == int(pedRc[0]): #pedRc[0] é o Id de um recurso solicitado em "ped"
                                rc.quantidade -= int(pedRc[1]) #pedRc[1] = quantidade de um recurso solicitado em "ped"
                                rc.indispMotivo = 'Usado em outra reserva'
                                if rc.quantidade < 0:
                                    rc.quantidade = 0

            #Template
            #Tipos de recurso para as TABS.
            #rcTipoRemoveVazio verifica se há Tipos de Recurso que não estão sendo usados em "recursos"
            recursoTipoLista = rcTipoRemoveVazio(list(recursoTipo.objects.filter(ativo=True)), recursos)

            pg.defPaginaTitulo("SELECIONE OS RECURSOS DO SEU PEDIDO")

            return render(request, 'pedidos/reservarSelRecursos.html', vars())


    if etapa == "resumo":

        recursoLista         = []
        recursoRequerOficio  = False
        podeReservarPorOutro = False

        if modo == "pr":
            recursos = request.POST.getlist('recurso')
            quants   = removeString0(request.POST.getlist('quant')) #Quantidades selecionadas de cada recurso

            #Tratamento de entradas com tamanho das listas de recurso e quants diferentes
            #O tamanho da lista de recursos e quants deve ser igual
            if len(recursos) != len(quants):
                messages.error(request, "Ouve um erro na criação do seu pedido. Por favor, tente novamente!")
                return redirect("/reservar/_/selModo/")

            #Tratamento de entrada com 0 recursos selecionados
            if len(recursos) == 0:
                messages.error(request, "Voce deve selecionar ao menos 1 recurso na lista de recursos. \
                                         Se não houver disponibilidade de recursos na lista, \
                                         verifique se há outras reservas ocupando o período selecionado.")

                return redirect("/reservar/_/selModo/")

            #Inserindo as quants nos objetos "rc"
            for index, rc in enumerate(recursos):
                recursoObj = recurso.objects.get(id=rc)
                recursoLista.append(recursoObj)
                recursoLista[index].quant = quants[index]
                #Verificando se algum recurso requer envio de Ofício
                if (recursoObj.requerOficio or request.user.usuario.tipo.requerOficio) and \
                   not verifPermissoes(request, permissao, [21]):
                    recursoRequerOficio = True

        elif modo == "rp":
            recursoObj = recurso.objects.get(id=request.POST["reservaRecursoId"], unidade__id=request.POST["unidadeId"])
            recursoLista.append(recursoObj)
            recursoLista[0].quant = 1 #Para o modo "rp", "quant" será sempre 1
            #Verificando se o recursoObj requer envio de Ofício
            #Usuários com permissão para Avaliar Pedidos (ID=21) não passam pela checagem da necessidade do envio de ofício
            if (recursoObj.requerOficio or request.user.usuario.tipo.requerOficio) and \
               not verifPermissoes(request, permissao, [21]):
                recursoRequerOficio = True


        # Verificando se o usuário pode reservar por outra pessoa
        if verifPermissoes(request, permissao, [31]):
            podeReservarPorOutro = True


        #Datas de início e fim formatadas para exibição
        try:
            reservaInicio = datetime.strptime(request.POST['reservaInicio'], "%Y-%m-%dT%H:%M:%S")
            reservaFim    = datetime.strptime(request.POST['reservaFim'], "%Y-%m-%dT%H:%M:%S")
        except ValueError:
            reservaInicio = datetime.strptime(request.POST['reservaInicio'], "%Y-%m-%d")
            reservaFim    = datetime.strptime(request.POST['reservaFim'],    "%Y-%m-%d")

        #Cálculo das horas usadas na seleção
        # dataDifer = reservaFim - reservaInicio
        # reservaHoras = dataDifer.days * 24 + dataDifer.seconds // 3600
        # reservaMinut = (dataDifer.seconds % 3600) // 60

        # xHoras = reservaHoras + (0 if (reservaMinut == 0) else (reservaMinut/60.0))

        unidadeObj = get_object_or_404(unidade,
                                       id=request.POST['unidadeId'],
                                       ativo=True,
                                       campus=request.user.usuario.campus)


        pg.defPaginaTitulo("RESUMO DO SEU PEDIDO")
        return render(request, 'pedidos/reservarResumo.html', vars())


    # Após clicar em "Confirmar Pedido"
    if etapa == "confirmacao":

        # Prevenção da tentativa de burlar a verificação do limite de pedidos ativos, pois esta verificação é feita
        # apenas na etapa inicial "selModo"
        # Expirando os pedidos com reservas já passadas
        expirarPedidos(request.user.usuario, pedido, pedidoStatus)

        # Verificando se o usuário já atingiu o limite de pedidos Ativos
        if usuarioPedLimite(request.user.usuario, pedido):
            messages.warning(request, "Seu pedido não foi concluído!")
            messages.warning(request, "<strong>Seu limite de pedidos ativos foi alcançado!</strong> \
                                       Por favor, faça uso da sua reserva mais próxima ou cancele uma reserva \
                                       para poder criar um novo Pedido.")

            return redirect('/painel/')

        #Verificando se o usuário aceitou os termos de uso
        if not request.POST.get('termosDeUsoAceito', False):
            messages.error(request, "Seu pedido não foi realizado! Você não aceitou os Termos de Uso! \
                                     Por favor, marque a opção \
                                     \"Declaro que li e aceito os Termos e Condições de Uso\" \
                                     na página de RESUMO do seu pedido")

            return redirect("/reservar/_/selModo/")

        # Configurações Gerais
        configUespi = configGeral.objects.get(id=1)

        # Solicitante
        if 'solicitanteNome' not in request.POST:
            solicitanteNome    = request.user.usuario.nome
            solicitanteContato = request.user.usuario.telefone
        else:
            solicitanteNome    = request.POST['solicitanteNome']
            solicitanteContato = request.POST['solicitanteContato']


        #Unidade selecionada
        unidadeObj = get_object_or_404(unidade,
                                       id=request.POST['unidadeId'],
                                       ativo=True,
                                       campus=request.user.usuario.campus)

        #Pegando os recursos selecionados pelo usuário
        recursos = request.POST.getlist('reservaRecursos')

        #Evitando entrada com 0 recursos selecionados
        if len(recursos) == 0:
            messages.error(request, "Voce deve selecionar ao menos 1 recurso na lista. \
                                     Se não houver disponibilidade de recursos na lista, \
                                     verifique se há outras reservas ocupando o período selecionado.")

            return redirect("/reservar/_/selModo/")

        #Lista paralela de quantidade da cada recurso selecionado
        recursosQuant = request.POST.getlist('reservaRecursosQuant')

        #Criando a lista de recursos do pedido. Uma string com o padrão ^([1-9]+,[1-9]+;)+[1-9]+,[1-9]+$
        pedRecursosLista    = []
        pedidoRequerAval    = False
        recursoRequerOficio = False
        for rc, rcQ in zip(recursos, recursosQuant):
            # Verificando se algum recurso selecionado requer avaliação dos avaliadores
            if pedidoRequerAval == False:
                rcObj = get_object_or_404(recurso, id=rc, unidade__id=request.POST['unidadeId'], ativo=True)
                if rcObj.requerAvaliacao:
                    pedidoRequerAval = True
                if rcObj.requerOficio:
                    recursoRequerOficio = True

            pedRecursosLista.append(rc+","+rcQ+",0,0,0,0") #(recursoId,quantidade,cedido,cedenteId,recebido,recebedorId)

        pedRecursosLista = ";".join(pedRecursosLista)
        
        #(recursoId,quantidade,cedido,cedenteId,recebido,recebedorId)
        if re.match('^([1-9]\d*,[1-9]\d*,0,0,0,0;)*[1-9]\d*,[1-9]\d*,0,0,0,0$', pedRecursosLista) is None:
            messages.error(request, "OCORREU UM ERRO NA CRIAÇÃO DO SEU PEDIDO.")
            messages.warning(request, "Por favor, tente novamente! Caso o erro persista, contate o suporte.")
            return redirect("/painel/")

        try:
            inicio = tz.localize(datetime.strptime(request.POST['reservaInicio'], "%Y-%m-%dT%H:%M:%S"))
            fim    = tz.localize(datetime.strptime(request.POST['reservaFim'],    "%Y-%m-%dT%H:%M:%S"))
        except ValueError:
            inicio = tz.localize(datetime.strptime(request.POST['reservaInicio'], "%Y-%m-%d"))
            fim    = tz.localize(datetime.strptime(request.POST['reservaFim'],    "%Y-%m-%d"))


        # Verificação dos recursos usados no pedidos "aceitos" e "em análise"
        # status do pedido | 1-Aceito, 3-Em Análise | pedidos criados até ano passado
        pedidos = pedido.objects.filter(status__id__in=[1,3],
                                        usuario__campus=request.user.usuario.campus,
                                        dataCriacao__year__gte=(datetime.today().year-1))

        #Bloqueando recursos que estão em outros pedidos de mesmo período escolhido no calendário
        for ped in pedidos:
            #Comparação do período do pedido "ped" com o período selecionado pelo usuário
            #Caso haja intersecção do período selecionado com o período de algum pedido já aceito ou em análise,
            #então é feita um subtração na quantidade de cada recurso que será disponibilizado para reserva
            if interseccao(inicio, fim, ped.reserva.inicio, ped.reserva.fim):
                for pedRc in ped.recursos.split(';'):
                    pedRc = map(int, pedRc.split(','))
                    for rc in pedRecursosLista.split(';'): #lista de recursos selecionados pelo usuário
                        rc = map(int, rc.split(','))
                        if rc[0] == pedRc[0]: #pedRc[0] é o Id de um recurso usado em "ped"
                            rc[1] -= pedRc[1] #pedRc[1] = quantidade de um recurso usado em "ped"
                            if rc[1] <= 0:
                                messages.warning(request, "Ops! Alguém chegou primeiro.")
                                messages.info(request, "Enquanto você criava seu pedido, outro usuário efetuou um pedido \
                                                        no mesmo horário.")
                                return redirect("/reservar/_/selModo/")

        #Para pedidos com descrição não obrigatória
        if "reservaDescricao" not in request.POST:
            request.POST['reservaDescricao'] = ""

        # Verificando se haverá necessidade de avaliação do pedido
        #ID=29 Reservar sem avaliação
        #ID=21 Avaliar pedidos de reserva
        if (verifPermissoes(request, permissao, [29]) and not pedidoRequerAval) or \
           (verifPermissoes(request, permissao, [29]) and verifPermissoes(request, permissao, [21])):
            reservaAtiva = True
            pedStatusId  = 1 #Aceito
            pedAvalData  = tz.localize(datetime.now())
            confirmMsg   = "Reservado!"
            confirmInstr = "Esse pedido já está Aceito! Não é necessário passar por avaliação."
        else:
            reservaAtiva = False
            pedStatusId  = 3 #Em Análise
            pedAvalData  = None
            confirmMsg   = "Reserva Solicitada!"
            confirmInstr = "Seu pedido está Em Análise! A avaliação do seu pedido leva de "+str(configUespi.periodoAvalInicio)+\
                           " a "+str(configUespi.periodoAvalFim)+" dias úteis."


        #Verificando se o usuário enviou o ofício
        oficioUpload = request.FILES.get('oficioSolicitacao', None)
        if not verifPermissoes(request, permissao, [21]):
            if (request.user.usuario.tipo.requerOficio or recursoRequerOficio) and oficioUpload is None:
                messages.error(request, "O ofício de solicitação não foi enviado!")
                messages.warning(request, "Seu pedido não foi concluído.")
                return redirect("/reservar/_/selModo/")

        #Verificando se o ofício enviado é maior que 10Mb
        if oficioUpload is not None and oficioUpload._size > 10485760:
            messages.error(request, "O ofício enviado é maior que 10Mb.")
            messages.warning(request, "Seu pedido não foi concluído.")
            return redirect("/reservar/_/selModo/")

        #Salvando os dados da Reserva
        res = reserva(
            titulo     = request.POST['reservaTitulo'],
            descricao  = request.POST['reservaDescricao'],
            inicio     = inicio,
            fim        = fim,
            diaInteiro = False, #!!! Implementar
            ativo      = reservaAtiva
        )
        res.save()

        #Salvando os dados do Pedido
        ped = pedido(
            usuario            = request.user.usuario,
            recursos           = pedRecursosLista,
            reserva            = res,
            campusUnidade      = unidadeObj,
            solicitanteNome    = solicitanteNome,
            solicitanteContato = solicitanteContato,
            dataAvaliacao      = pedAvalData,
            status             = pedidoStatus.objects.get(id=pedStatusId)
        )
        ped.save()

        if oficioUpload is not None:
            #Salvando o ofício de solicitação
            pasta = os.path.join(settings.BASE_DIR, 'DHSite/static/media/pedidos/oficios/')
            oficioUpload.name = codGerador(5)+"-ofício-N"+str(ped.id)+os.path.splitext(oficioUpload.name)[1]

            #Cria uma nova pasta, caso ela não exista
            if not os.path.exists(pasta):
                os.makedirs(pasta)

            oficioArq = open(pasta+oficioUpload.name, 'wb+')
            for chunk in oficioUpload.chunks():
                oficioArq.write(chunk)
            oficioArq.close()

            ped.oficio = pasta.replace(os.path.join(settings.BASE_DIR, 'DHSite/static/'), '')+oficioUpload.name
            ped.save()

        #Notificando na plataforma os usuários com permissão de avaliar pedidos sobre o novo pedido
        notfic = notificacao(
            descricao        = "{"+request.user.usuario.nome+"} solicitou uma reserva",
            usuarioOrigem    = request.user.usuario,
            permissaoDestino = permissao.objects.get(id=21), #ID=21 Avaliar Pedidos de Reserva
            link             = '/solicitacoes/detalhar/'+str(ped.id)+'/',
            campus           = request.user.usuario.campus
        )
        notfic.save()

        if not reservaAtiva:
            #Notificando por email os usuários com permissão de avaliar pedidos sobre o novo pedido
            emailLista  = []
            usuarioQS   = User.objects.filter(usuario__campus=request.user.usuario.campus, is_active=True)
            for us in usuarioQS: 
                if '21' in carregPermissoes(us.usuario): # ID=21 Avaliar Pedidos de Reserva
                    emailLista.append(str(us.email))

            send_mail(
                subject='Novo pedido de Reserva!',
                message=render_to_string('emails/novoPedido.txt', vars()),
                from_email='nao-responda@plataformadohere.com.br',
                recipient_list=emailLista,
                fail_silently=True,
                html_message=render_to_string('emails/novoPedido.html', vars()),
            )

        return render(request, 'pedidos/reservarConfirmacao.html', vars())



#LISTA DE RECURSOS DISPONÍVEIS #################################################################################################
@login_required
@never_cache
def recursosView(request, modo, acao, recursoId):

    # Verificando as permissões do usuário
    if not verifPermissoes(request, permissao, [24]): #ID=24 Acessar Recursos Disponíveis
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Recursos", paginaTitulo="LISTA DE RECURSOS")


    if modo == 'listar':

        #Permissão de Entrada para edição de um recurso
        if verifPermissoes(request, permissao, [23]): #ID=23 Controle de Recursos
            recursos = recurso.objects.filter(unidade__campus=request.user.usuario.campus)
            dataHref = "/recursos/editar"
            podeCadastrar = True
        else:
            #Filtrando os recursos a serem mostrados ao usuário que tenha permissão apenas de visualizar
            recursosLista = list(recurso.objects.filter(unidade__campus=request.user.usuario.campus, ativo=True))
            recursos = []
            #Criando a lista dos recursos disponíveis para o tipo do usuário solicitante
            for rc in recursosLista:
                if unicode(request.user.usuario.tipo.id) in rc.disponibUsuario.split(','):
                    recursos.append(rc)

            dataHref = "/recursos/exibir"

        #para o Template
        unidadeLista = unidade.objects.filter(ativo=True, campus=request.user.usuario.campus)

        return render(request, 'recursos/recursosLista.html', vars())


    if modo == 'exibir':
        #Pegando o recurso no banco
        try:
            recursoObj = recurso.objects.get(id=int(recursoId), ativo=True)
        except recurso.DoesNotExist:
            messages.error(request, "Este recurso não existe ou foi desativado!")
            return redirect('/recursos/listar/')

        recursoFotos = recursoObj.fotos.split(";")
        semFoto = recursoFotos.count('vazio') == len(recursoFotos)

        pg.defPaginaTitulo(recursoObj.nome)
        return render(request, 'recursos/recurso.html', vars())


    if modo == 'editar':
        if not verifPermissoes(request, permissao, [23]): #ID=23 Controle de Recurso
            raise Http404()

        #Pegando o recurso no banco
        try:
            recursoObj = recurso.objects.get(id=int(recursoId), unidade__campus=request.user.usuario.campus)
        except recurso.DoesNotExist:
            messages.error(request, "Este recurso não existe ou foi desativado!")
            return redirect('/recursos/listar/')


        if acao == 'salvar': #Entrada ao clicar em "Salvar"

            # VALIDAÇÃO BÁSICA DAS ENTRADAS
            # Valor da quantidade
            if int(request.POST['recursoQuantidade']) <= 0:
                messages.error(request, "A quantidade do recurso deve ser um número maior que 0!")
                return redirect('/recursos/editar/'+recursoId+'/')

            # Validação do formato dos horários de disponibilidade início e fim
            if not re.match("^$|^(([01][0-9])|(2[0-3])):[0-5][0-9]$", request.POST['recursoDispHoraInicio']) or \
               not re.match("^$|^(([01][0-9])|(2[0-3])):[0-5][0-9]$", request.POST['recursoDispHoraFim']):
                messages.error(request, "O Horário de disponibilidade informado é inválido!")
                return redirect('/recursos/editar/'+recursoId+'/')

            # O horário de Início não deve ser igual ou superior ao horário de Fim
            recursoDispHoraInicio = int(request.POST['recursoDispHoraInicio'].replace(":", ""))
            recursoDispHoraFim    = int(request.POST['recursoDispHoraFim'].replace(":", ""))
            if (recursoDispHoraInicio > recursoDispHoraFim):
                messages.error(request, "Horário de Disponibilidade: O horário de Início deve ser inferior ao horário do Fim")
                return redirect('/recursos/editar/'+recursoId+'/')

            try:
                recursoTipoObj = recursoTipo.objects.get(id=int(request.POST.get('recursoTipoId', 0)),
                                                         ativo=True)
                unidadeObj = unidade.objects.get(id=int(request.POST.get('recursoUnidadeId', 0)),
                                                 campus=request.user.usuario.campus,
                                                 ativo=True)
            except recursoTipo.DoesNotExist:
                messages.error(request, "O Tipo de Recurso informado não é válido!")
                return redirect('/recursos/editar/'+recursoId+'/')
            except unidade.DoesNotExist:
                messages.error(request, "A Unidade informada não é válida!")
                return redirect('/recursos/editar/'+recursoId+'/')


            #Salvando as fotos do recurso
            foto1     = request.FILES.get('recursoFotoInput1', None)
            foto2     = request.FILES.get('recursoFotoInput2', None)
            foto3     = request.FILES.get('recursoFotoInput3', None)
            fotoLista = [foto1, foto2, foto3]

            for img in fotoLista:
                if img is not None and img._size > 2097152:
                    messages.error(request, "Ao menos uma foto é maior que 2Mb")
                    return redirect('/recursos/editar/'+recursoId+'/')

            imagemPasta = os.path.join(settings.BASE_DIR, 'DHSite/static/imagens/recursos/'+str(recursoObj.id)+'/')

            #Cria uma nova pasta, caso ela não exista
            if not os.path.exists(imagemPasta):
                os.makedirs(imagemPasta)

            # Fotos já registradas do rescurso
            fotoUrlLista = recursoObj.fotos.split(';')

            for idx, img in enumerate(fotoLista):
                if img is not None:
                    #Verificando se a existência de uma foto já salva
                    #Caso exista, esta é então deletada
                    if os.path.isfile(os.path.join(settings.BASE_DIR, 'DHSite/static/'+fotoUrlLista[idx])):
                        os.remove(os.path.join(settings.BASE_DIR, 'DHSite/static/'+fotoUrlLista[idx]))
                    #Criando um nome para o arquivo e verificando se este já existe 
                    while True:
                        img.name = codGerador(5)+os.path.splitext(img.name)[1]
                        if not os.path.isfile(imagemPasta+img.name):
                            break
                    #Salvando a foto no servidor
                    arquivo = open(imagemPasta+img.name, 'wb+')
                    for chunk in img.chunks():
                        arquivo.write(chunk)
                    arquivo.close()
                    #Adicionando o endereço da nova foto na lista de fotos do recurso
                    fotoUrlLista[idx] = "imagens/recursos/"+str(recursoObj.id)+"/"+img.name #Endereco da foto na pasta "static/"


            recursoObj.nome            = request.POST['recursoNome']
            recursoObj.tipo            = recursoTipoObj
            recursoObj.descricao       = request.POST['recursoDescricao']
            recursoObj.fotos           = ';'.join(fotoUrlLista)
            recursoObj.quantidade      = request.POST['recursoQuantidade']
            recursoObj.unidade         = unidadeObj
            recursoObj.dispHoraInicio  = request.POST['recursoDispHoraInicio']
            recursoObj.dispHoraFim     = request.POST['recursoDispHoraFim']
            recursoObj.disponibUsuario = '1,2,'+','.join(request.POST.getlist('disponibUsuario'))
            recursoObj.ativo           = True if 'recursoAtivo' in request.POST else False
            recursoObj.requerAvaliacao = True if 'recursoRequerAval' in request.POST else False
            recursoObj.requerOficio    = True if 'recursoRequerOficio' in request.POST else False
            recursoObj.save()

            #Salvando esta atividade no Log
            logAtiv = log(
                atividade = "ALTERAÇÃO no recurso ID="+str(recursoObj.id)+", Nome="+recursoObj.nome,
                usuario   = request.user.usuario
            )
            logAtiv.save()

            notfic = notificacao(
                descricao        = "{"+request.user.usuario.nome+"} editou o recurso \""+recursoObj.nome+"\"",
                usuarioOrigem    = request.user.usuario,
                permissaoDestino = permissao.objects.get(id=23), #ID=23 Controlar Recursos
                link             = '/recursos/exibir/'+str(recursoObj.id)+'/',
                campus           = recursoObj.unidade.campus
            )
            notfic.save()

            messages.success(request, "Alteração realizada com sucesso!")

            return redirect('/recursos/editar/'+recursoId+'/')

        else: #Carregamento do recurso para edição

            unidades = unidade.objects.filter(campus__id=request.user.usuario.campus.id, ativo=True) #Unidades do campus
            fotos    = recursoObj.fotos.split(';')

            # Setando a disponibilidade do recurso para cada tipo de usuário
            recursoDisp = recursoObj.disponibUsuario.split(',')
            rcTipos     = recursoTipo.objects.filter(ativo=True)
            usTipos     = usuarioTipo.objects.filter(ativo=True)\
                                             .exclude(id__in=[1,2])\
                                             .order_by('id') #Usado no template
            for usTipo in usTipos:
                usTipo.recursoDisp = True if (str(usTipo.id) in recursoDisp) else False

            pg.defPaginaTitulo("Editar - "+recursoObj.nome)

            return render(request, 'recursos/recursosAdic.html', vars())


    # CADASTRO DE NOVO RECURSO | BOTÃO "ADICIONAR NOVO RECURSO"
    if modo == 'adicionar':
        if not verifPermissoes(request, permissao, [23]): #ID=23 Controle de Recurso
                raise Http404()

        if acao == 'salvar': #Entrada ao clicar em "Adicionar"

            # VALIDAÇÃO BÁSICA DAS ENTRADAS
            # Valor da quantidade
            if int(request.POST['recursoQuantidade']) <= 0:
                messages.error(request, "A quantidade do recurso deve ser um número maior que 0!")
                return redirect('/recursos/adicionar/')

            # Validação do formato dos horários de disponibilidade início e fim
            if not re.match("^$|^(([01][0-9])|(2[0-3])):[0-5][0-9]$", request.POST['recursoDispHoraInicio']) or \
               not re.match("^$|^(([01][0-9])|(2[0-3])):[0-5][0-9]$", request.POST['recursoDispHoraFim']):
                messages.error(request, "O Horário de disponibilidade informado é inválido!")
                return redirect('/recursos/adicionar/')

            # O horário de Início não deve ser igual ou superior ao horário de Fim
            if int(request.POST['recursoDispHoraInicio'].replace(":", "")) > \
               int(request.POST['recursoDispHoraFim'].replace(":", "")):
                messages.error(request, "Horário de Disponibilidade: O horário de Início deve ser inferior ao horário do Fim")
                return redirect('/recursos/adicionar/')

            try:
                recursoTipoObj = recursoTipo.objects.get(id=int(request.POST.get('recursoTipoId', 0)),
                                                         ativo=True)
                unidadeObj = unidade.objects.get(id=int(request.POST.get('recursoUnidadeId', 0)),
                                                 campus=request.user.usuario.campus,
                                                 ativo=True)
            except recursoTipo.DoesNotExist:
                messages.error(request, "O Tipo de Recurso informado não é válido!")
                return redirect('/recursos/adicionar/')
            except unidade.DoesNotExist:
                messages.error(request, "A Unidade informada não é válida!")
                return redirect('/recursos/adicionar/')


            #Salvando o recurso
            recursoObj = recurso(
                nome            = request.POST['recursoNome'],
                tipo            = recursoTipoObj,
                descricao       = request.POST['recursoDescricao'],
                fotos           = "vazio;vazio;vazio",
                quantidade      = request.POST['recursoQuantidade'],
                disponibUsuario = '1,2,'+','.join(request.POST.getlist('disponibUsuario')),
                dispHoraInicio  = request.POST['recursoDispHoraInicio'],
                dispHoraFim     = request.POST['recursoDispHoraFim'],
                unidade         = unidadeObj,
                requerAvaliacao = True if 'recursoRequerAval' in request.POST else False,
                requerOficio    = True if 'recursoRequerOficio' in request.POST else False,
                ativo           = False
            )
            recursoObj.save()

            # Salvando as imagens no servidor
            #!!! VERIFICAR AS EXTENSÕES VÁLIDAS
            foto1 = request.FILES.get('recursoFotoInput1', None)
            foto2 = request.FILES.get('recursoFotoInput2', None)
            foto3 = request.FILES.get('recursoFotoInput3', None)
            fotoLista = [foto1, foto2, foto3]

            for img in fotoLista:
                if img is not None and img._size > 2097152:
                    messages.error(request, "Ao menos uma foto é maior que 2Mb")
                    return redirect('/recursos/editar/'+recursoId+'/')

            imagemPasta = os.path.join(settings.BASE_DIR, 'DHSite/static/imagens/recursos/'+str(recursoObj.id)+'/')
            if not os.path.exists(imagemPasta):
                os.makedirs(imagemPasta)

            # Lista com espaços vazios para os endereços das fotos
            fotoUrlLista = ['vazio', 'vazio', 'vazio']

            for idx, img in enumerate(fotoLista):
                if img is not None:
                    #Criando um nome para o arquivo e verificando se este já existe 
                    while True:
                        img.name = codGerador(5)+os.path.splitext(img.name)[1]
                        if not os.path.isfile(imagemPasta+img.name):
                            break
                    #Salvando a foto no servidor
                    arquivo  = open(imagemPasta+img.name, 'wb+')
                    for chunk in img.chunks():
                        arquivo.write(chunk)
                    arquivo.close()

                    fotoUrlLista[idx] = imagemPasta.replace(os.path.join(settings.BASE_DIR, "DHSite/static/"), "")+img.name

            # Inserindo o endereço das imagens no recurso
            recursoObj.fotos = ";".join(fotoUrlLista)
            recursoObj.ativo = True if 'recursoAtivo' in request.POST else False
            recursoObj.save()

            #Salvando esta atividade no Log
            logAtiv = log(
                atividade = "CADASTRO do recurso ID="+str(recursoObj.id)+", Nome="+recursoObj.nome,
                usuario   = request.user.usuario
            )
            logAtiv.save()

            notfic = notificacao(
                descricao        = "{"+request.user.usuario.nome+"} adicionou o recurso \""+recursoObj.nome+"\"",
                usuarioOrigem    = request.user.usuario,
                permissaoDestino = permissao.objects.get(id=23), #Controlar Recursos
                link             = '/recursos/exibir/'+str(recursoObj.id)+'/',
                campus           = recursoObj.unidade.campus
            )
            notfic.save()

            messages.success(request, "Recurso adicionado!")

        else:
            # Página com campos em branco
            unidades = unidade.objects.filter(campus__id=request.user.usuario.campus.id, ativo=True)
            fotos    = ['vazio', 'vazio', 'vazio']
            rcTipos  = recursoTipo.objects.filter(ativo=True)
            usTipos  = usuarioTipo.objects.filter(ativo=True)\
                                          .exclude(id__in=[1,2])\
                                          .order_by('id') #Usado no template

            pg.defPaginaTitulo("ADICIONAR RECURSO")

            return render(request, 'recursos/recursosAdic.html', vars())

    return redirect("/recursos/listar/")


# DADOS PESSOAIS ############################################################################################################
@login_required
def usuarioDados(request, modo, acao):

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Dados Cadastrais", paginaTitulo="DADOS CADASTRAIS")

    hoje = datetime.now().strftime('%d-%m-%Y')

    if modo in ['alterarSenha', 'primeiroAcesso']:

        if modo == 'primeiroAcesso' and request.user.usuario.endereco is not None:
            raise Http404()

        if acao == 'salvar':

            user = User.objects.get(username=request.user.username)

            if modo == 'alterarSenha':
                #verifica senha atual
                if not user.check_password(request.POST['senhaAtual']):
                    messages.error(request, 'Senha atual incorreta!')
                    return redirect('/usuarioDados/alterarSenha/')

            #verifica senhas diferentes
            if not verif_senha_dupla(request.POST['senhaNova'], request.POST['senhaNova2']):
                messages.error(request, "As senhas digitadas estão diferentes! Tente novamente")
                if modo == 'alterarSenha':
                    return redirect('/usuarioDados/alterarSenha/')
                else:
                    return redirect('/usuarioDados/primeiroAcesso/')

            #Teste de senha fraca
            try:
                validate_password(request.POST['senhaNova'])
            except ValidationError:
                messages.error(request, "A senha que você digitou é muito fraca! Por favor, digite outra senha.")
                if modo == 'alterarSenha':
                    return redirect('/usuarioDados/alterarSenha/')
                else:
                    return redirect('/usuarioDados/primeiroAcesso/')

            #Salvando a nova senha
            user.set_password(request.POST['senhaNova'])
            user.save()
            update_session_auth_hash(request, user)

            #Salvando esta atividade no Log
            logAtiv = log(
                atividade = "ALTERAÇÃO DA SENHA",
                usuario   = request.user.usuario
            )
            logAtiv.save()

            if modo == 'primeiroAcesso':
                messages.success(request, "Senha alterada! Agora preencha seus Dados Cadastrais.")
                return redirect('/usuarioDados/editar/')

            messages.success(request, "Senha alterada com sucesso!")
            return redirect('/painel/')

        if modo == 'primeiroAcesso':
            return render(request, 'usuario/primeiroAcesso.html', vars())

        return render(request, 'usuario/dados/alterarSenha.html', vars())


    if modo == 'editar':

        usuarioObj = request.user.usuario

        if acao == 'salvar':
            usuarioObj.nome      = request.POST['nome']
            usuarioObj.dataNasc  = request.POST['nasc']
            usuarioObj.telefone  = request.POST['telefone']
            usuarioObj.sexo      = None if 'sexoId'    not in request.POST else sexo.objects.get(id=request.POST['sexoId'])
            usuarioObj.profissao = None if 'profissao' not in request.POST else request.POST['profissao']
            usuarioObj.curso     = None if 'cursoId'   not in request.POST else curso.objects.get(id=request.POST['cursoId'])
            usuarioObj.setor     = None if 'setorId'   not in request.POST else setor.objects.get(id=int(request.POST['setorId']))

            #Tratamento do Endereço
            try:
                usuarioObj.endereco.cep
            except AttributeError:
                end = endereco(
                    cep     = request.POST['endCep'],
                    rua     = request.POST['endRua'],
                    numero  = request.POST['endNum'],
                    complem = request.POST['endCompl'],
                    bairro  = request.POST['endBairro'],
                    cidade  = request.POST['endCidade'],
                    uf      = request.POST['endUf'],
                    ativo   = True
                )
                end.save()
                usuarioObj.endereco = end

            else:
                usuarioObj.endereco.cep     = request.POST['endCep']
                usuarioObj.endereco.rua     = request.POST['endRua']
                usuarioObj.endereco.numero  = request.POST['endNum']
                usuarioObj.endereco.complem = request.POST['endCompl']
                usuarioObj.endereco.bairro  = request.POST['endBairro']
                usuarioObj.endereco.cidade  = request.POST['endCidade']
                usuarioObj.endereco.uf      = request.POST['endUf']
                usuarioObj.endereco.save() #Salvando o Endereço no BD

            #Salvando os dados do usuário no BD
            usuarioObj.save()

            #Salvando esta atividade no Log
            logAtiv = log(
                atividade = "ALTERAÇÃO NOS DADOS CADASTRAIS",
                usuario   = request.user.usuario
            )
            logAtiv.save()

            if 'primeiroAcesso' in request.POST:
                messages.success(request, "Pronto! Seus dados foram cadastrados com sucesso. Seja bem-vindo(a) à \
                                        plataforma de reservas da UESPI!")
                return redirect('/painel/')

            messages.success(request, "Dados salvos com sucesso!")
            return redirect('/usuarioDados/editar/')


        # Entrada na página
        setores = setor.objects.filter(campus=request.user.usuario.campus, ativo=True)\
                               .order_by('nome') #Template

        cursos  = curso.objects.filter(campus=request.user.usuario.campus, ativo=True)\
                               .order_by('nome') #Template

        usuarioObj.dataNasc = unicode(usuarioObj.dataNasc) #Template

        #Lista de inputs para o tipo do usuário
        usInputLista = request.user.usuario.tipo.dadosInputLista
        
        return render(request, 'usuario/dados/usuarioDados.html', vars())


# ESTATÍSTICAS #############################################################################################################
@login_required
def estatisticas(request, campusId, modo):

    if not verifPermissoes(request, permissao, [16]): # ID=16 Acessar Estatísticas Locais
        raise Http404()

    # Verificando se o usuário tem permissão para alternar entre os campus
    altCampus = alternaCampus(request, permissao, campus, campusId)
    if altCampus is None and campusId is not None:
        return redirect('/estatisticas/')

    campusAtual = request.user.usuario.campus if altCampus is None else altCampus['campusAtual']

    #Entrada por requisição AJAX
    if modo == 'carregJSON':

        pedStatus = pedidoStatus.objects.all().order_by('id')

        # Gráfico Anual - Barras Verticais -------------------------------------------------------------------------
        hoje = tz.localize(datetime.today()).date()
        umAnoAtras = tz.localize(datetime.strptime(str(hoje.year-1)+'-'+str(hoje.month+1)+'-01', "%Y-%m-%d")).date()

        mesLista        = []
        anualDadosSt    = []
        anualDados      = []
        suggestedMaxAno = 0

        for status in pedStatus:
            for x in range(0, 12):
                add = x
                ano = umAnoAtras.year
                mes = umAnoAtras.month+add
                if mes > 12:
                    add -= 12
                    ano += 1

                if status.id == 1: #primeira repetição
                    mesLista.append(mesListaStr[umAnoAtras.month+add-1])

                anualDadosSt.append(pedido.objects.filter(usuario__campus=campusAtual,
                                                          dataCriacao__month=umAnoAtras.month+add,
                                                          dataCriacao__year=ano,
                                                          status=status).count())
                if max(anualDadosSt) > suggestedMaxAno:
                    suggestedMaxAno = max(anualDadosSt)

            anualDados.append({
                'label': status.nome,
                'data': anualDadosSt,
                'backgroundColor': status.cor
            })
            anualDadosSt = []


        #----------------------------------------------------------------------------------------------------------


        # Gráfico de Barras mês atual por dia ---------------------------------------------------------------------

        mesAtualDados = []
        diaLista      = []
        mesSemPedidos = True
        for dia in range(1, calendar.monthrange(hoje.year, hoje.month)[1]+1):
            diaLista.append(dia)
            diaRange = [
                tz.localize(datetime(hoje.year, hoje.month, dia, 0, 0, 0)),
                tz.localize(datetime(hoje.year, hoje.month, dia, 23, 59, 59))
            ]
            pedidosDiaMes = pedido.objects.filter(usuario__campus=campusAtual, dataCriacao__range=diaRange).count()
            if mesSemPedidos and pedidosDiaMes > 0:
                mesSemPedidos = False

            mesAtualDados.append(pedido.objects.filter(usuario__campus=campusAtual, dataCriacao__range=diaRange).count())

        suggestedMaxMes = 1 if mesSemPedidos else int(floor(max(mesAtualDados)+(max(mesAtualDados)/100.0)*10))


        #----------------------------------------------------------------------------------------------------------

        # Gráfico de Pizza (Pie) de todos os pedidos --------------------------------------------------------------
        pedAceitos = pedido.objects.filter(usuario__campus=campusAtual, status__id=1).count()
        pedNegados = pedido.objects.filter(usuario__campus=campusAtual, status__id=2).count()
        pedAnalise = pedido.objects.filter(usuario__campus=campusAtual, status__id=3).count()
        pedCancela = pedido.objects.filter(usuario__campus=campusAtual, status__id=4).count()

        # Lista de nome e cores dos status dos pedidos
        statusCores = []
        statusNomes = []
        for status in pedStatus:
            statusCores.append(status.cor)
            statusNomes.append(status.nome)

        #----------------------------------------------------------------------------------------------------------


        # lista de Gráficos Estatísticos em formato JSON
        graficoLista = {
            'anualBarras': gerarGraficoObj(
                graficoTipo  = 'bar',
                datasets     = anualDados,
                labels       = mesLista,
                suggestedMax = int(floor(suggestedMaxAno+(suggestedMaxAno/100.0)*10))
            ),

            'mesAtualBarras': gerarGraficoObj(
                graficoTipo  = 'bar',
                datasets     = [{
                                   'label': 'Pedidos realizados',
                                   'data': mesAtualDados,
                                   'backgroundColor': '#0077db'
                               }],
                labels       = diaLista,
                suggestedMax = suggestedMaxMes
            ),

            'totalPizza': gerarGraficoObj(
                graficoTipo  = 'pie',
                datasets     = [{
                                   'data': [pedAceitos, pedNegados, pedAnalise, pedCancela],
                                   'backgroundColor': statusCores,
                                    'borderWidth': False
                               }],
                labels       = statusNomes
            )
        }

        return JsonResponse(graficoLista, safe=False)

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Estatísticas", paginaTitulo="ESTATÍSTICAS")
    
    return render(request, 'estatisticas.html', vars())


#LISTA DE RESERVAS DO USUÁRIO #############################################################################################

@login_required
def minhasReservas(request, modo, acao, pedidoId):

    if not verifPermissoes(request, permissao, [19]): # ID=19 Acessar Minhas Reservas
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Minhas Reservas", paginaTitulo="MINHAS RESERVAS")

    # Expirando os pedidos com reservas já passadas
    expirarPedidos(request.user.usuario, pedido, pedidoStatus)


    if modo == 'detalhar':
        try:
            pedidoObj = pedido.objects.get(id=pedidoId, usuario=request.user.usuario)
        except pedido.DoesNotExist:
            messages.error(request, "Acesso Negado!")
            return redirect('/minhasReservas/listar/')

        pedRecursos = []

        for pedRc in pedidoObj.recursos.split(";"):
            pedRc = pedRc.split(',')
            pedRecursos.append({'nome':recurso.objects.get(id=pedRc[0]).nome, 'quant':pedRc[1]})

        #Zerando as notificações de pedidos avaliados

        if acao == 'cancelar':

            if (pedidoObj.status.id == 4): #status 'Cancelado'
                messages.error(request, "Este pedido já foi cancelado.")
                return redirect('/minhasReservas/detalhar/'+pedidoId)

            if (pedidoObj.status.id == 2): #status 'Negado'
                messages.error(request, "Não é possível cancelar.")
                return redirect('/minhasReservas/detalhar/'+pedidoId)

            #Mudando o status do pedido
            pedidoObj.status        = pedidoStatus.objects.get(id=4) #Status "Cancelado"
            pedidoObj.reserva.ativo = False

            pedidoObj.save()
            pedidoObj.reserva.save()
            messages.warning(request, "Pedido cancelado!")

            notfic = notificacao(
                descricao        = "{"+request.user.usuario.nome+"} cancelou um pedido",
                usuarioOrigem    = request.user.usuario,
                permissaoDestino = permissao.objects.get(id=21), #ID=21 Avaliar pedidos de reserva
                link             = "/solicitacoes/detalhar/"+str(pedidoObj.id)+"/",
                campus           = request.user.usuario.campus
            )
            notfic.save()

            #Notificando por email os usuários com permissão de avaliar pedidos sobre o pedido cancelado
            emailLista  = []
            usuarioQS   = User.objects.filter(usuario__campus=request.user.usuario.campus, is_active=True)
            for us in usuarioQS: 
                if '21' in carregPermissoes(us.usuario): # ID=21 Avaliar Pedidos de Reserva
                    emailLista.append(str(us.email))

            send_mail(
                subject='Pedido de reserva CANCELADO!',
                message=render_to_string('emails/pedidoCancelado.txt', vars()),
                from_email='nao-responda@plataformadohere.com.br',
                recipient_list=emailLista,
                fail_silently=True,
                #html_message=render_to_string('emails/pedidoCancelado.html', vars())
            )

            return redirect('/minhasReservas/detalhar/'+pedidoId)

        podeCancelarIds = [1,3]

        return render(request, 'pedidos/minhaReservaDetalhes.html', vars())

    if modo == 'listar':
        pedidosQS = pedido.objects.filter(usuario=request.user.usuario).order_by('-id')

        #PAGINAÇÃO DOS PEDIDOS
        paginacao = Paginator(pedidosQS, 50)

        try:
            pedidos = paginacao.page(request.GET.get('pag'))
        except PageNotAnInteger:
            pedidos = paginacao.page(1)
        except EmptyPage:
            pedidos = paginacao.page(paginacao.num_pages)

        return render(request, 'pedidos/minhasReservas.html', vars())


# LISTA DE SOLICITAÇÕES DOS USUÁRIOS ########################################################################################
@login_required
def solicitacoes(request, modo, acao, pedidoId):

    # Verificando as permissões do usuário
    if not verifPermissoes(request, permissao, [20]): # ID=20 Acessar Pedidos de Reserva
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Solicitacoes", paginaTitulo="SOLICITAÇÕES")


    if modo == 'detalhar':

        try:
            pedidoObj = pedido.objects.get(id=pedidoId, usuario__campus=request.user.usuario.campus)
        except pedido.DoesNotExist:
            messages.error(request, "Acesso Negado!")
            return redirect('/minhasReservas/listar/')

        pedRecursos = []

        for pedRc in pedidoObj.recursos.split(";"):
            pedRc = pedRc.split(',')
            pedRecursos.append({'nome':recurso.objects.get(id=pedRc[0]).nome, 'quant':pedRc[1]})

        if acao is not None:
            
            if not verifPermissoes(request, permissao, [21]): #ID=21 Avaliar Pedidos de Reserva
                messages.error(request, "Você não tem permissao para Avaliar Pedidos de Reserva")
                return redirect('/solicitacoes/detalhar/'+pedidoId+'/')

            #Caso o pedido tenha sido cancelado
            if pedidoObj.status.id == 4:
                messages.error(request, "Este pedido foi cancelado pelo solicitante.")
                return redirect('/solicitacoes/detalhar/'+pedidoId+'/')

            #Caso o pedido tenha sido Avaliado, ou seja, tenha o status "Aceito" ou "Negado"
            if pedidoObj.status.id in [1,2]:
                messages.error(request, "Este pedido já foi avaliado!")
                return redirect('/solicitacoes/detalhar/'+pedidoId+'/')

            statusAnterior = pedidoObj.status.nome

            #Mudando o status dos pedidos
            if acao == "aceitar":
                pedidoObj.status        = pedidoStatus.objects.get(id=1)  #Aceito
                pedidoObj.reserva.ativo = True
                pedidoObj.observacao    = request.POST["observacao"]
                pedidoObj.dataAvaliacao = tz.localize(datetime.now())
                messages.success(request, "Pedido aceito!")

            elif acao == "negar":
                pedidoObj.status        = pedidoStatus.objects.get(id=2)  #Negado
                pedidoObj.reserva.ativo = False
                pedidoObj.observacao    = request.POST["observacao"]
                pedidoObj.dataAvaliacao = tz.localize(datetime.now())
                messages.success(request, "Pedido negado!")

            pedidoObj.save()
            pedidoObj.reserva.save()

            #Salvando esta atividade no Log
            logAtiv = log(
                atividade = "PEDIDO AVALIADO! ID="+str(pedidoObj.id)+", Título="+pedidoObj.reserva.titulo+" ("\
                             +statusAnterior+" -> "+pedidoObj.status.nome+")",
                usuario   = request.user.usuario
            )
            logAtiv.save()

            # Notificação para o solicitante
            notfic1 = notificacao(
                descricao       = "Seu pedido de reserva {"+pedidoObj.reserva.titulo+"} foi "+pedidoObj.status.nome,
                usuarioOrigem   = request.user.usuario,
                usuarioDestino  = pedidoObj.usuario,
                link            = "/minhasReservas/detalhar/"+str(pedidoObj.id)+"/",
                campus          = request.user.usuario.campus
            )
            notfic1.save()

            # Notificação para os avaliadores do campus
            notfic2 = notificacao(
                descricao        = "{"+request.user.usuario.nome+"} avaliou o pedido \""\
                                   +pedidoObj.reserva.titulo+"\": {"\
                                   +pedidoObj.status.nome+"}",
                usuarioOrigem    = request.user.usuario,
                permissaoDestino = permissao.objects.get(id=21), #ID=21 Avaliar Pedidos de Reserva
                link             = "/solicitacoes/detalhar/"+str(pedidoObj.id)+"/",
                campus           = request.user.usuario.campus
            )
            notfic2.save()

            #Email para o usuário solicitante
            send_mail(
                subject        = 'Pedido de reserva AVALIADO!',
                message        = render_to_string('emails/pedidoAvaliado.txt', vars()),
                from_email     = 'nao-responda@plataformadohere.com.br',
                recipient_list = [pedidoObj.usuario.user.email],
                fail_silently  = True,
                #html_message   = render_to_string('emails/pedidoAvaliado.html', vars())
            )

            return redirect('/solicitacoes/detalhar/'+pedidoId+'/')

        podeAvaliarIds = [3] #ID=3 Em Análise

        return render(request, 'pedidos/solicitacaoDetalhes.html', vars())


    if modo == 'listar':

        # Expirando os pedidos com reservas já passadas
        expirarPedidos(request.user.usuario, pedido, pedidoStatus, campus=True)
        
        #Para o template
        pedidosQS = pedido.objects.filter(usuario__campus=request.user.usuario.campus).order_by('-id')[:100]

        #PAGINAÇÃO DOS PEDIDOS
        paginacao = Paginator(pedidosQS, 50)

        try:
            pedidos = paginacao.page(request.GET.get('pag'))
        except PageNotAnInteger:
            pedidos = paginacao.page(1)
        except EmptyPage:
            pedidos = paginacao.page(paginacao.num_pages)

        return render(request, 'pedidos/solicitacoes.html', vars())



#ADMINISTRADOR: LISTA DE USUÁRIOS ###########################################################################################
@login_required
def usuariosView(request, modo, usuarioId, acao):
    
    if not verifPermissoes(request, permissao, [9]): # ID=9 Cadastrar Usuário
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Usuários", paginaTitulo="USUÁRIOS")

    #Opções de visualização e edição dos usuários
    if modo == 'listar':

        if verifPermissoes(request, permissao, [8]): #ID=8 Desativar Administrador #!!!
            usuariosQS = User.objects.all()\
                                     .exclude(usuario=request.user.usuario)\
                                     .order_by('-id')[:100]

        elif verifPermissoes(request, permissao, [10]): # ID=10 Desativar Usuário
            usuariosQS = User.objects.filter(usuario__campus=request.user.usuario.campus)\
                                     .exclude(usuario=request.user.usuario)\
                                     .order_by('-id')[:100]

        #PAGINAÇÃO DOS USUÁRIOS
        paginacao = Paginator(usuariosQS, 50)

        try:
            usuarios = paginacao.page(request.GET.get('pag'))
        except PageNotAnInteger:
            usuarios = paginacao.page(1)
        except EmptyPage:
            usuarios = paginacao.page(paginacao.num_pages)

        return render(request, 'usuario/usuariosLista.html', vars())

    # Exibição dos dados do usuário com opções de desativar ou reativar
    if modo == 'exibir':
        if verifPermissoes(request, permissao, [8]): # ID=8 Desativar Administrador
            usuarioObj = get_object_or_404(User, usuario__id=usuarioId)
        elif verifPermissoes(request, permissao, [10]): # ID=10 Desativar Usuário
            usuarioObj = get_object_or_404(User, usuario__id=usuarioId, usuario__campus=request.user.usuario.campus)

        # Se o usuário a ser exibido for o próprio solicitante, então redireciona para os dados cadastrais
        if usuarioObj == request.user:
            return redirect("/usuarios/listar/")

        # Entrada com a acao de desativar ou reativar usuário
        if acao is not None:
            if (usuarioObj.usuario.tipo == request.user.usuario.tipo) and\
               (usuarioObj.usuario.id < request.user.usuario.id):
               messages.error(request, "Você não pode ativar/desativar este usuário!")
               return redirect("/usuarios/listar/")

            if acao == 'desativar':
                if usuarioObj.is_active:
                    usuarioObj.is_active = False
                    usuarioObj.save()
                    messages.success(request, 'Usuário Desativado!')
                    atividade = "DESATIVADA"

            elif acao == 'reativar':
                if not usuarioObj.is_active:
                    usuarioObj.is_active = True
                    usuarioObj.save()
                    messages.success(request, 'Usuário Reativado!')
                    atividade = "REATIVADA"

            #Salvando a atividade no Log
            logAtiv = log(
                atividade = "CONTA DE USUÁRIO "+atividade+" ID="+str(usuarioObj.usuario.id)+", Nome="+usuarioObj.usuario.nome,
                usuario   = request.user.usuario
            )
            logAtiv.save()

            # Notificação
            notfic = notificacao(
                descricao        = "A conta do usuário {"+usuarioObj.usuario.nome+"} foi "+atividade+\
                                   " por "+request.user.usuario.nome+" ("+request.user.usuario.tipo.nome+")",
                usuarioOrigem    = request.user.usuario,
                usuarioExcluido  = usuarioObj.usuario,
                permissaoDestino = permissao.objects.get(id=6), #Controle de usuários
                link             = "/usuarios/exibir/"+str(usuarioObj.usuario.id)+"/",
                campus           = usuarioObj.usuario.campus
            )
            notfic.save()

            return redirect('/usuarios/exibir/'+str(usuarioObj.usuario.id)+'/')

        return render(request, 'usuario/usuario.html', vars())


# OUTROS CADASTROS ##########################################################################################################
@login_required
def outrosCadastros(request):

    if not verifPermissoes(request, permissao, [25]): # ID=25 Acessar Outros Cadastros
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Outros Cadastros", paginaTitulo="OUTROS CADASTROS")

    tabelas = []

    if verifPermissoes(request, permissao, [26]): # ID=26 Controlar Cursos
        tabelas.append('CURSO')
    if verifPermissoes(request, permissao, [27]): # ID=26 Controlar Setores
        tabelas.append('SETOR')
    if verifPermissoes(request, permissao, [12]): # ID=26 Controlar Feriados Locais
        tabelas.append('FERIADO')

    return render(request, 'outrosCadastros/outrosCadastros.html', vars())



# OUTROS CADASTROS | CURSO #################################################################################################
@login_required
def cursoView(request, modo, acao, cursoId):
    
    if not verifPermissoes(request, permissao, [26]): # ID=26 Controlar Cursos
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Outros Cadastros", paginaTitulo="CURSOS")

    if modo == 'adicionar':

        if acao == "salvar":

            cursoObj = curso(
                nome   = request.POST['curso'],
                campus = request.user.usuario.campus,
                ativo  = True if 'cursoAtivo' in request.POST else False
            )
            
            cursoObj.save()
            messages.success(request, "Curso Cadastrado!")

            return redirect('/curso/listar/')

        pg.defPaginaTitulo("ADICIONAR CURSO")
        return render(request, 'outrosCadastros/curso/adicionar.html', vars())

    if modo == 'editar':

        if acao == 'salvar':

            cursoObj = get_object_or_404(curso, id=cursoId, campus=request.user.usuario.campus)
            cursoObj.nome  = request.POST['curso']
            cursoObj.ativo = True if 'cursoAtivo' in request.POST else False

            cursoObj.save()
            messages.success(request, "Alteração Salva!")

            return redirect('/curso/editar/'+cursoId+'/')

        cursoEdit = get_object_or_404(curso, id=cursoId, campus=request.user.usuario.campus)
        pg.defPaginaTitulo("EDITAR CURSO")
        return render(request, 'outrosCadastros/curso/adicionar.html', vars())

    if modo == 'listar':
        # LISTA DE CURSOS
        cursos = curso.objects.filter(campus=request.user.usuario.campus).order_by("nome")
        return render(request, 'outrosCadastros/curso/lista.html', vars())

    raise Http404()


# OUTROS CADASTROS | SETOR #################################################################################################

@login_required
def setorView(request, modo, acao, setorId):

    if not verifPermissoes(request, permissao, [27]): # ID=27 Controlar Setores
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Outros Cadastros", paginaTitulo="SETORES")

    if modo == 'adicionar':

        if acao == "salvar":

            setorObj = setor(
                nome   = request.POST['setor'],
                campus = request.user.usuario.campus,
                ativo  = True if 'setorAtivo' in request.POST else False
            )
            
            setorObj.save()
            messages.success(request, "Setor Cadastrado!")
            return redirect('/setor/listar/')

        pg.defPaginaTitulo("ADICIONAR SETOR")
        return render(request, 'outrosCadastros/setor/adicionar.html', vars())

    if modo == 'editar':

        if acao == 'salvar':

            setorObj = get_object_or_404(setor, id=setorId, campus=request.user.usuario.campus)
            setorObj.nome  = request.POST['setor']
            setorObj.ativo = True if 'setorAtivo' in request.POST else False

            setorObj.save()
            messages.success(request, "Alteração Salva!")
            return redirect('/setor/editar/'+setorId+'/')

        setorEdit = get_object_or_404(setor, id=setorId, campus=request.user.usuario.campus)
        pg.defPaginaTitulo("EDITAR SETOR")
        return render(request, 'outrosCadastros/setor/adicionar.html', vars())
    
    if modo == 'listar':
        # LISTA DE SETORES
        setores = setor.objects.filter(campus=request.user.usuario.campus).order_by("nome")
        return render(request, 'outrosCadastros/setor/lista.html', vars())

    raise Http404()


# OUTROS CADASTROS | FERIADO #################################################################################################

@login_required
def feriadoView(request, modo, acao, feriadoId):

    if not verifPermissoes(request, permissao, [12]): #ID=12 Controlar Feriados Locais
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Outros Cadastros", paginaTitulo="FERIADOS")

    if modo == 'adicionar':

        if acao == "salvar":

            feriadoObj = feriado(
                data   = request.POST['feriadoData'],
                nome   = request.POST['feriadoNome'],
                campus = request.user.usuario.campus,
                ativo  = True if 'feriadoAtivo' in request.POST else False
            )
            
            feriadoObj.save()
            messages.success(request, "Feriado Cadastrado!")
            return redirect('/feriado/listar/')

        pg.defPaginaTitulo("ADICIONAR FERIADO")
        return render(request, 'outrosCadastros/feriado/adicionar.html', vars())

    if modo == 'editar':
        feriadoObj = get_object_or_404(feriado, id=feriadoId, campus=request.user.usuario.campus)

        if acao == 'salvar':
            feriadoObj.data  = request.POST['feriadoData']
            feriadoObj.nome  = request.POST['feriadoNome']
            feriadoObj.ativo = True if 'feriadoAtivo' in request.POST else False

            feriadoObj.save()
            messages.success(request, "Alteração Salva!")
            return redirect('/feriado/editar/'+feriadoId+'/')

        feriadoObj.data = feriadoObj.data.strftime("%Y-%m-%d")
        pg.defPaginaTitulo("EDITAR FERIADO")
        return render(request, 'outrosCadastros/feriado/adicionar.html', vars())

    if modo == 'listar':
        # LISTA DE FERIADOS
        feriados = feriado.objects.filter(Q(campus=request.user.usuario.campus)|Q(campus=None)).order_by("data")
        return render(request, 'outrosCadastros/feriado/lista.html', vars())

    raise Http404()


# RELATAR PROBLEMA ###########################################################################################################

def relatarProblema(request):

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Relatar Problema", paginaTitulo="RELATAR PROBLEMA")
    
    return render(request, 'rodape/relatarProblema.html', vars())


# TERMOS DE USO #############################################################################################################

def termos(request):

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Termos", paginaTitulo="TERMOS DE USO")
    
    return render(request, 'rodape/termos.html', vars())


# DÚVIDAS FREQUENTES ########################################################################################################

def duvidasFrequentes(request):

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Dúvidas", paginaTitulo="DÚVIDAS FREQUENTES")

    return render(request, 'rodape/duvidasFrequentes.html', vars())


# DESENVOLVEDORES ###########################################################################################################

def desenvolvedores(request):

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Desenvolvedores", paginaTitulo="DESENVOLVEDORES")

    return render(request, 'rodape/desenvolvedores.html', vars())


# POLÍTICAS DE PRIVACIDADE ##################################################################################################

def politicasDePrivacidade(request):

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Políticas de Privacidade", paginaTitulo="POLÍTICAS DE PRIVACIDADE")

    return render(request, 'rodape/politicasDePrivacidade.html', vars())


# CONTATO ###################################################################################################################

def contato(request):

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Contato", paginaTitulo="CONTATO")

    return render(request, 'rodape/contato.html', vars())


# SAIBA MAIS #################################################################################################################

def saibaMais(request):

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Saiba Mais", paginaTitulo="SAIBA MAIS")

    return render(request, 'saibaMais.html', vars())


# GERENCIAMENTO DO CAMPI ####################################################################################################

@login_required
def campusView(request, modo, acao, campusId):
    
    if not verifPermissoes(request, permissao, [13]): # ID=13 Controlar Campi
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Campus", paginaTitulo="CAMPI")

    if modo == 'adicionar':

        if acao == "salvar":

            campusObj = campus(
                nome    = request.POST['campusNome'],
                direcao = request.POST['campusDirecao'],
                ativo   = False
            )
            campusObj.save()

            enderecoObj = endereco(
                cep     = request.POST['endCep'],
                rua     = request.POST['endRua'],
                numero  = request.POST['endNum'],
                complem = request.POST['endCompl'],
                bairro  = request.POST['endBairro'],
                cidade  = request.POST['endCidade'],
                uf      = request.POST['endUf'],
                ativo   = True
            )
            enderecoObj.save()

            unidadeObj = unidade(
                nome     = "Sede do Campus",
                telefone = request.POST['telefone'],
                endereco = enderecoObj,
                campus   = campusObj,
                sede     = True,
                ativo    = True if 'campusAtivo' in request.POST else False
            )
            unidadeObj.save()

            campusObj.ativo = True if 'campusAtivo' in request.POST else False
            campusObj.save()
            
            messages.success(request, "Campus Cadastrado!")
            return redirect('/campus/listar/')

        pg.defPaginaTitulo("ADICIONAR CAMPUS")
        return render(request, 'campus/adicionar.html', vars())

    if modo == 'editar':

        if acao == 'salvar':

            campusObj         = get_object_or_404(campus, id=campusId)
            campusObj.nome    = request.POST['campusNome']
            campusObj.direcao = request.POST['campusDirecao']
            campusObj.ativo   = True if 'campusAtivo' in request.POST else False
            campusObj.save()

            unidadeObj                  = get_object_or_404(unidade, campus=campusObj, sede=True)
            unidadeObj.nome             = "Sede do Campus"
            unidadeObj.telefone         = request.POST['telefone']
            unidadeObj.endereco.cep     = request.POST['endCep']
            unidadeObj.endereco.rua     = request.POST['endRua']
            unidadeObj.endereco.numero  = request.POST['endNum']
            unidadeObj.endereco.complem = request.POST['endCompl']
            unidadeObj.endereco.bairro  = request.POST['endBairro']
            unidadeObj.endereco.cidade  = request.POST['endCidade']
            unidadeObj.endereco.uf      = request.POST['endUf']
            unidadeObj.endereco.save()
            unidadeObj.save()

            messages.success(request, "Alteração Salva!")
            return redirect('/campus/editar/'+campusId+'/')

        campusEdit  = get_object_or_404(campus, id=campusId)
        unidadeEdit = unidade.objects.get(campus=campusEdit, sede=True)
        pg.defPaginaTitulo("EDITAR CAMPUS")
        return render(request, 'campus/adicionar.html', vars())
    
    if modo == 'listar':
        # LISTAR CAMPI
        campusQS  = campus.objects.all()
        unidadeQS = unidade.objects.filter(sede=True)

        for u in unidadeQS:
            for c in campusQS:
                if u.campus == c:
                    c.cidade = u.endereco.cidade

        return render(request, 'campus/lista.html', vars())

    raise Http404()


# MEU CAMPUS | UNIDADES #################################################################################################

@login_required
def unidadeView(request, modo, acao, unidadeId):

    if not verifPermissoes(request, permissao, [14]): # ID=14 Controlar Meu Campus
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Unidades do Campus", paginaTitulo="UNIDADES")

    if modo == 'adicionar':

        if acao == "salvar":

            enderecoObj = endereco(
                cep     = request.POST['endCep'],
                rua     = request.POST['endRua'],
                numero  = request.POST['endNum'],
                complem = request.POST['endCompl'],
                bairro  = request.POST['endBairro'],
                cidade  = request.POST['endCidade'],
                uf      = request.POST['endUf'],
                ativo   = True
            )
            enderecoObj.save()

            unidadeObj = unidade(
                nome     = request.POST['unidadeNome'],
                telefone = request.POST['telefone'],
                endereco = enderecoObj,
                campus   = request.user.usuario.campus,
                sede     = False,
                ativo    = True if 'unidadeAtiva' in request.POST else False
            )
            unidadeObj.save()
            
            messages.success(request, "Unidade Cadastrada!")
            return redirect('/meuCampus/')

        pg.defPaginaTitulo("ADICIONAR UNIDADE")
        return render(request, 'campus/unidade/adicionar.html', vars())

    if modo == 'editar':

        if acao == 'salvar':

            unidadeObj = get_object_or_404(unidade, id=unidadeId, campus=request.user.usuario.campus)
            unidadeObj.nome             = request.POST['unidadeNome']
            unidadeObj.telefone         = request.POST['telefone']
            unidadeObj.ativo            = True if 'unidadeAtiva' in request.POST else False

            unidadeObj.endereco.cep     = request.POST['endCep']
            unidadeObj.endereco.rua     = request.POST['endRua']
            unidadeObj.endereco.numero  = request.POST['endNum']
            unidadeObj.endereco.complem = request.POST['endCompl']
            unidadeObj.endereco.bairro  = request.POST['endBairro']
            unidadeObj.endereco.cidade  = request.POST['endCidade']
            unidadeObj.endereco.uf      = request.POST['endUf']
            unidadeObj.endereco.ativo   = True

            unidadeObj.endereco.save()
            unidadeObj.save()
            messages.success(request, "Alteração Salva!")
            return redirect('/unidade/editar/'+unidadeId+'/')

        unidadeEdit = get_object_or_404(unidade, id=unidadeId, campus=request.user.usuario.campus)

        if unidadeEdit.sede:
            return redirect("/meuCampus/")

        pg.defPaginaTitulo("EDITAR UNIDADE")
        return render(request, 'campus/unidade/adicionar.html', vars())

    raise Http404()

# Meu Campus ###############################################################################################################

@login_required
def meuCampus(request, campusId):

    if not verifPermissoes(request, permissao, [14]): # ID=14 Controlar Meu Campus
        raise Http404()

    # Verificando se o usuário tem permissão para alternar entre os campus
    altCampus = alternaCampus(request, permissao, campus, campusId)
    if altCampus is None and campusId is not None:
        return redirect('/meuCampus/')

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Meu Campus", paginaTitulo="MEU CAMPUS")

    if altCampus is not None:
        meuCampus = altCampus['campusAtual']
    else:
        meuCampus = request.user.usuario.campus

    unidadeQS = unidade.objects.filter(campus=meuCampus)

    return render(request, 'campus/meuCampus.html', vars())


# Feriados #################################################################################################################

@login_required
def feriados(request):
    
    return render(request, 'feriados.html', vars())


# SUPERADMIN Feriados #######################################################################################################

@login_required
def configuracoes(request):

    # Definições da página
    pg = pagina(request, abaTitulo="DoHere | Configurações", paginaTitulo="CONFIGURAÇÕES")

    logAcesso   = True if verifPermissoes(request, permissao, [4]) else False #ID=4 Acesso ao Log
    geralAcesso = True if verifPermissoes(request, permissao, [3]) else False #ID=3 Acesso às Config Gerais

    return render(request, 'configuracoes.html', vars())


# LOG DO SISTEMA #########################################################################################################

@login_required
def logView(request):

    if not verifPermissoes(request, permissao, [4]): # ID=4 Acessar ao Log
        raise Http404()

    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Log", paginaTitulo="LOG DE ATIVIDADES")

    logLista = log.objects.filter(usuario__campus=request.user.usuario.campus).order_by('-dataCriacao')

    #PAGINAÇÃO DA DOS LOGS
    paginacao = Paginator(logLista, 50)

    try:
        logs = paginacao.page(request.GET.get('pag'))
    except PageNotAnInteger:
        logs = paginacao.page(1)
    except EmptyPage:
        logs = paginacao.page(paginacao.num_pages)
    
    return render(request, 'log.html', vars())


# LOG DO SISTEMA #########################################################################################################

@login_required
def notificacoes(request, modo, ntfId, link):

    usPermissoes = carregPermissoes(request.user.usuario)

    if modo == 'link':
        try:
            notific = notificacao.objects.get(id=ntfId)
        except notificacao.DoesNotExist:
            messages.error(request, "Há um erro no link da notificação.")
            return redirect('/notificacoes/verTudo/')

        if (not notific.visto) and notificarDestino(request.user.usuario, notific, usPermissoes):
            notific.visto    = True
            # notific.vistoPor = str(request.user.usuario.id)
            notific.save()

        # elif str(request.user.usuario.id) not in notific.vistoPor.split(','):
        #     notific.vistoPor += ","+str(request.user.usuario.id)
        #     notific.save()

        return redirect('/'+link+'/')

    if modo == 'carregamento':
        # Notificações destinadas ao usuário solicitante ou a usuários que tenham permissão para recebê-las
        notificacoes = notificacao.objects.exclude(usuarioOrigem=request.user.usuario)\
                                          .filter (campus=request.user.usuario.campus,
                                                   visto=False,
                                                   dataCriacao__gt=request.user.usuario.dataCriacao)

        notificacoesLista = []

        for ntf in notificacoes:
            if notificarDestino(request.user.usuario, ntf, usPermissoes):
                #Preenchimento e preparação da notificacoesLista para ser enviada como JSON
                #ntf.descricao = (ntf.descricao[:100] + '..') if len(ntf.descricao) > 100 else ntf.descricao
                ntf.descricao = ntf.descricao.replace("{", "<strong>").replace("}", "</strong>")
                notificacoesLista.append({
                    'id'       : ntf.id,
                    'descricao': ntf.descricao,
                    'dataHora' : ntf.dataCriacao.astimezone(tz).strftime("%d/%m - %H:%M"),
                    'link'     : ntf.link
                })

        return JsonResponse(notificacoesLista, safe=False)

    if modo == 'verTudo':
        notificacoesQs = notificacao.objects.exclude(usuarioOrigem=request.user.usuario)\
                                            .filter(campus=request.user.usuario.campus,
                                                    dataCriacao__gt=request.user.usuario.dataCriacao)\
                                            .order_by('-id')
        notifLista = []

        for ntf in notificacoesQs:
            if notificarDestino(request.user.usuario, ntf, usPermissoes):
                #substituição dos caracteres "{" e "}" por "<strong>" e "</strong>"
                ntf.descricao = ntf.descricao.replace('{','<strong>').replace('}','</strong>')
                notifLista.append(ntf)


        #PAGINAÇÃO DAS NOTIFICAÇÕES
        paginacao = Paginator(notifLista, 50)

        try:
            notificacoes = paginacao.page(request.GET.get('pag'))
        except PageNotAnInteger:
            notificacoes = paginacao.page(1)
        except EmptyPage:
            notificacoes = paginacao.page(paginacao.num_pages)


    #Definições da página
    pg = pagina(request, abaTitulo="DoHere | Notificações", paginaTitulo="NOTIFICAÇÕES")
    
    return render(request, 'notificacoes.html', vars())


# Reservas Retorno em Json ##################################################################################################

@login_required
def reservaJSON(request, pedidoStatus, recursoId, campusId):
    
    # Dados "start" e "end" do fullCalendar para filtrar os pedidos
    try:
        inicio = tz.localize(datetime.strptime(request.GET['start'], "%Y-%m-%dT%H:%M:%S"))
        fim    = tz.localize(datetime.strptime(request.GET['end'],   "%Y-%m-%dT%H:%M:%S"))
    except ValueError:
        inicio = tz.localize(datetime.strptime(request.GET['start'], "%Y-%m-%d"))
        fim    = tz.localize(datetime.strptime(request.GET['end'],   "%Y-%m-%d"))


    if recursoId is not None and recursoId != "_": #Calendário de reservas no modo RP
        pedidosQS = pedido.objects.filter(Q(reserva__inicio__range=[inicio,fim]) | Q(reserva__fim__range=[inicio,fim]),
                                          status__id__in=[1,3],
                                          usuario__campus=request.user.usuario.campus)
        pedidos   = []
        for ped in pedidosQS:
            pedRecursos = ped.recursos.split(';')
            for pedRc in pedRecursos:
                pedRc = pedRc.split(',')
                if recursoId == pedRc[0]:
                    pedidos.append(ped)

    else: #Calendário de reservas no modo PR ou Calendário Estático
        if verifPermissoes(request, permissao, [1]) and campusId is not None: # ID=1 Alternar entre os campi
            try:
                campusObj = campus.objects.get(id=campusId, ativo=True)
            except campus.DoesNotExist:
                messages.error(request, "Selecione um Campus válido na lista!")
                return redirect("/calendario/")
        else:
            campusObj = request.user.usuario.campus

        pedidos = pedido.objects.filter(Q(reserva__inicio__range=[inicio,fim]) | Q(reserva__fim__range=[inicio,fim]),
                                        usuario__campus=campusObj)


    #O fullCalendar necessita da data pura (sem as horas) para exibir um evento allDay (Dia Inteiro)
    #zeroHora será comparada com as horas das reservas vindas do banco
    #Caso haja zeroHora nessas instâncias, somente a data será enviada ao FullCalendar
    zeroHora = datetime(2000,1,1,0,0,0).time()

    reservaLista = []

    if pedidoStatus is not None:
        for ped in pedidos:
            ped.reserva.inicio = ped.reserva.inicio.astimezone(tz)
            ped.reserva.fim    = ped.reserva.fim.astimezone(tz)

            if (ped.status.id == int(pedidoStatus)): # pedidoStatus | 1-Aceito, 2-Negado, 3-Em Análise, 4-Cancelado

                recursos = "<strong>"+ped.campusUnidade.nome+"</strong><br>"
                for pedRc in ped.recursos.split(';'):
                    pedRc = pedRc.split(',')
                    try:
                        rcNome = recurso.objects.get(id=pedRc[0], ativo=True).nome
                    except recurso.DoesNotExist:
                        rcNome = "Recurso Inativo"

                    recursos += pedRc[1]+"x "+rcNome+"<br>"

                #Adicionando o status "Em Análise" no título do evento
                if (ped.status.id == 3):
                    ped.reserva.titulo += "\n("+ped.status.nome+")"

                #Jogando os pedidos na lista
                if (ped.reserva.inicio).time() == zeroHora and (ped.reserva.fim).time() == zeroHora:
                    reservaLista.append({
                        'title': ped.reserva.titulo,
                        'start': (ped.reserva.inicio).date().isoformat(),
                        'end'  : (ped.reserva.fim).date().isoformat(),
                        'description': recursos
                    })

                else:
                    reservaLista.append({
                        'title'      : ped.reserva.titulo,
                        'start'      : (ped.reserva.inicio).isoformat(),
                        'end'        : (ped.reserva.fim).isoformat(),
                        'description': recursos
                    })


    return JsonResponse(reservaLista, safe=False)


# Feriados Retorno em Json #################################################################################################

@login_required
def feriadoJSON(request, campusId):

    if verifPermissoes(request, permissao, [1]): # ID=1 Alternar entre os campi
        try:
            campusObj = campus.objects.get(id=campusId, ativo=True)
        except campus.DoesNotExist:
            messages.error(request, "Selecione um Campus válido na lista!")
            return redirect("/calendario/")
    else:
        campusObj = request.user.usuario.campus


    feriados     = feriado.objects.filter(ativo=True, campus=campusObj)
    feriadoLista = []

    for fer in feriados:
        fer.data = fer.data.astimezone(tz)
        # feriadoLista.append({
        #     'start'    : fer.data.date().isoformat(),
        #     'end'      : (fer.data + timedelta(days=1)).date().isoformat(),
        #     'rendering': 'background',
        #     'allDay'   : 'true'
        # })
        feriadoLista.append({
            'title' : fer.nome,
            'start' : fer.data.date().isoformat(),
            'end'   : (fer.data + timedelta(days=1)).date().isoformat(),
            'allDay': 'true'
        })
        # feriadoLista.append({
        #     'start'    : fer.data.isoformat(),
        #     'end'      : (fer.data + timedelta(days=1)).isoformat(),
        #     'rendering': 'background',
        # })

    return JsonResponse(feriadoLista, safe=False)


# Configurações do FullCalendar
def calendarioConfigJSON(request, recursoId):
    try:
        recursoObj = recurso.objects.get(id=recursoId, ativo=True)
        dispHoraInicio = recursoObj.dispHoraInicio.strftime("%H:%M:%S")
        dispHoraFim    = recursoObj.dispHoraFim.strftime("%H:%M:%S")

        #Adaptação para o FullCalendar
        if dispHoraFim == "00:00:00":
            dispHoraFim = "24:00:00"

        config = {"horaInicio" : dispHoraInicio, "horaFim" : dispHoraFim}

    except recurso.DoesNotExist:
        config = {"horaInicio":"falha", "horaFim":"falha"}

    return JsonResponse(config)

