# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.admin import UserAdmin
from django.contrib import admin
from DHSite import models


#Configuração da exibição das tabelas no painel de Admin


class pedidoStatusAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class painelTemaAdmin(admin.ModelAdmin):
    list_display       = ('id', 'titulo', 'arquivoCss', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('titulo',)

class usuarioTipoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class recursoTipoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class enderecoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'cep', 'rua', 'numero', 'complem', 'bairro', 'cidade', 'uf', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('rua',)

class campusAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'direcao', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class unidadeAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'endereco', 'campus', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class cursoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'campus', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class setorAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'campus', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class sexoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class usuarioAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'tipo', 'campus', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class recursoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'tipo', 'unidade', 'quantidade', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class reservaAdmin(admin.ModelAdmin):
    list_display       = ('id', 'titulo', 'inicio', 'fim', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('titulo',)

class pedidoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'reserva', 'usuario', 'status', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('reserva',)

class feriadoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'nome', 'data', 'campus', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('nome',)

class painelOpcaoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'titulo', 'link', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('titulo',)

class permissaoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'descricao', 'ativo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('descricao',)

class notificacaoAdmin(admin.ModelAdmin):
    list_display       = ('id', 'descricao', 'usuarioOrigem', 'visto', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('descricao',)

class logAdmin(admin.ModelAdmin):
    list_display       = ('id', 'atividade', 'usuario', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('atividade',)

class configGeralAdmin(admin.ModelAdmin):
    list_display       = ('id', 'instituicaoNomeCurto', 'sistemaAtivo', 'dataCriacao', 'dataModificacao',)
    list_display_links = ('instituicaoNomeCurto',)




#Registro das tabelas na página do Django Admin
admin.site.register(models.pedidoStatus, pedidoStatusAdmin)
admin.site.register(models.painelTema, painelTemaAdmin)
admin.site.register(models.usuarioTipo, usuarioTipoAdmin)
admin.site.register(models.recursoTipo, recursoTipoAdmin)
admin.site.register(models.endereco, enderecoAdmin)
admin.site.register(models.campus, campusAdmin)
admin.site.register(models.unidade, unidadeAdmin)
admin.site.register(models.curso, cursoAdmin)
admin.site.register(models.setor, setorAdmin)
admin.site.register(models.sexo, sexoAdmin)
admin.site.register(models.usuario, usuarioAdmin)
admin.site.register(models.recurso, recursoAdmin)
admin.site.register(models.reserva, reservaAdmin)
admin.site.register(models.pedido, pedidoAdmin)
admin.site.register(models.feriado, feriadoAdmin)
admin.site.register(models.painelOpcao, painelOpcaoAdmin)
admin.site.register(models.permissao, permissaoAdmin)
admin.site.register(models.notificacao, notificacaoAdmin)
admin.site.register(models.log, logAdmin)
admin.site.register(models.configGeral, configGeralAdmin)
