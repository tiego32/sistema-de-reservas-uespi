#encoding: utf-8
from django.core.exceptions     import ObjectDoesNotExist
from datetime                   import datetime, timedelta
from django.contrib             import messages
import re
import base64
import string
import random
import pytz

# Constantes
tz = pytz.timezone("America/Fortaleza")

# Identifica intersecção entre dois períodos de Datetime
def interseccao(inicio1, fim1, inicio2, fim2):
    return ((inicio1 >  inicio2) and (inicio1 <  fim2)) \
        or ((fim1    >  inicio2) and (fim1    <  fim2)) \
        or ((inicio1 <= inicio2) and (fim1    >= fim2))


# Cria uma lista de recursos para facilitar a exibição desse no Template
# Retorna uma cópia do pedidoQS passado com a adição do atributo recursoLista do tipo dict
def criarRcLista(recursoModel, pedidoQS):
    for ped in pedidoQS:
        ped.recursosLista = []
        for rc in ped.recursos.split(";"):
            rc    = rc.split(",")
            rc[0] = recursoModel.objects.get(id=int(rc[0]))
            rc[0].quant     = rc[1]
            # rc[0].cedido    = boolTrueFalseStr(rc[2])
            # rc[0].cedente   = rc[3]
            # rc[0].recebido  = boolTrueFalseStr(rc[4])
            # rc[0].recebedor = rc[5]
            ped.recursosLista.append(rc[0])

    return pedidoQS


# Verificação do Limite de pedidos Ativos do usuário
def usuarioPedLimite(usuario, pedido):
    #Contagem dos pedidos ativos (Não expirados com status = Aceito ou Em Análise)
    usPedidosAtivos = pedido.objects.filter(usuario=usuario,
                                            expirado=False,
                                            status__id__in=[1,3]).count()

    # Tipos de usuário com "pedidoMax" = 0 não têm limite de pedidos
    # retorna "True" se o limite foi alcançado, caso contrário "False"
    return (usuario.tipo.pedidosMax > 0) and (usPedidosAtivos >= usuario.tipo.pedidosMax)            


# Expira os pedidos com o período de reserva já passado
def expirarPedidos(usuario, pedido, pedidoStatus, campus=False):
    # Expirando todos os pedidos do campus do usuário ou de um usuário específico
    if campus:
        pedidosQs = pedido.objects.filter(expirado=False, usuario__campus=usuario.campus)
    else:
        pedidosQs = pedido.objects.filter(expirado=False, usuario=usuario)

    # Dia e hora em que esta função é chamada
    hojeAgora = tz.localize(datetime.today())

    # Expirando os pedidos com reservas já passadas
    for ped in pedidosQs:
        if ped.reserva.fim.astimezone(tz) < hojeAgora:
            ped.expirado = True

            if ped.status.id == 3:
                ped.status        = pedidoStatus.objects.get(id=2)
                ped.dataAvaliacao = hojeAgora
                ped.reserva.ativo = False
                ped.observacao    = "Este pedido foi negado automaticamente pelo sistema devido ao período de reserva \
                                     requisitado ter expirado."
            ped.reserva.save()
            ped.save()


# Verifica se há Tipos de Recurso que não estão sendo usados em "recursos"
def rcTipoRemoveVazio(rcTipos, recursos):
    novoRcTipos = []
    for rcTipo in rcTipos:
        for rc in recursos:
            if rc.tipo == rcTipo:
                if rcTipo not in novoRcTipos:
                    novoRcTipos.append(rcTipo)

    return novoRcTipos


def alternaCampus(request, permissaoModel, campusModel, campusId):
    if verifPermissoes(request, permissaoModel, [1]): # ID=1 Alternar nos Campi
        campusLista = campusModel.objects.filter(ativo=True)
        campusAtual = request.user.usuario.campus

        if campusId is not None:
            try:
                campusAtual = campusModel.objects.get(id=campusId, ativo=True)
            except campusModel.DoesNotExist:
                messages.error(request, "Selecione um Campus válido na lista!")
                return None

        return {'campusLista': campusLista, 'campusAtual': campusAtual}

    return None


def verifPermissoes(request, permissaoModel, permissaoIdLista):
    # SuperAdmin's (ID=1) têm acesso total no sistema
    if request.user.usuario.tipo.id == 1:
        return True

    # Verificando se a permissão de cadastro de usuários está ativa
    #<objeto Model permissao, id da permissão a ser verificada>
    if not permissaoAtiva(permissaoModel, permissaoIdLista):
        messages.error(request, "Essa funcionalidade está desativada!")
        return False

    # Carregando as Permissões do usuário
    usPermissoes = carregPermissoes(request.user.usuario)

    # Bloqueio da entrada de usuários comuns
    # Se usuário não tem permissão para cadastrar usuário, então bloquear entrada!
    for permissaoId in permissaoIdLista:
        if str(permissaoId) not in usPermissoes:
            return False

    return True

# retorno uma lista com as permissões do usuário
# <Usuário request>
def carregPermissoes(usuario):
    if usuario.permissoes is None:
        return usuario.tipo.permissoes.split(',')
    else:
        return usuario.permissoes.split(',')


#Verifica se uma lista de permissões estão ativas
def permissaoAtiva(permissaoModel, permissaoIdLista):
    for permissaoId in permissaoIdLista:
        try:
            permissaoModel.objects.get(id=permissaoId, ativo=True)
        except permissaoModel.DoesNotExist:
            return False

    return True


def notificarDestino(usuarioRq, notif, usPermissoes):
    if (((notif.permissaoDestino is None) and (notif.usuarioDestino == usuarioRq)) or\
       ((notif.usuarioDestino is None) and (str(notif.permissaoDestino.id) in usPermissoes))):
       return True
    else:
        return False


def removeString0(lista):
    while "0" in lista:
        lista.remove("0")
    return lista


def carregPainelOpcoes(usPermissoes, permissao, painelOpcao):
    painelOpcaoLista = []
    painelOpcaoQS    = painelOpcao.objects.all().order_by('precedencia')
    permissaoQS      = permissao.objects.filter(ativo=True)\
                                        .filter(id__in=usPermissoes)\
                                        .exclude(painelOpcao__isnull=True)

    for painelOpcaoObj in painelOpcaoQS:
        for permissaoObj in permissaoQS:
            if painelOpcaoObj == permissaoObj.painelOpcao:
                painelOpcaoLista.append(painelOpcaoObj)
        
    return painelOpcaoLista

#Loop contando os dias de um período de tempo
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)


#Gerador de números aleatórios (strings)
def codGerador(tam):
    return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(tam))


#Encriptação de strings
def encode(key, clear):
    enc = []
    for i in range(len(clear)):
        key_c = key[i % len(key)]
        enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
        enc.append(enc_c)
    return base64.urlsafe_b64encode("".join(enc))

#Decriptação de strings
def decode(key, enc):
    dec = []
    enc = base64.urlsafe_b64decode(enc)
    for i in range(len(enc)):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
        dec.append(dec_c)
    return "".join(dec)


# Substituição "Espaço-Underscore" e "Underscore-Espaço" ########################################################
def espaco_underscore(origStr):
    return origStr.replace(" ", "_")

def underscore_espaco(origStr):
    return origStr.replace("_", " ")


def convParaDjangoUserName(nome, nomeLista):
    dUserName = espaco_underscore(nome)
    i = 2

    while dUserName in nomeLista:
        if i == 2:
            dUserName = dUserName + "_@" + str(i)
        else:
            dUserName = dUserName.replace("_@" + str(i-1), "_@" + str(i))
        i += 1

        if i > 10:
            return None

    return dUserName



#Configuração geral das páginas ##################################################################################
class pagina():
    abaTitulo = "DoHere" #Título na aba
    paginaTitulo = "DOHERE"

    navbar = { #Opções na barra de navegação
        "logo" : "imagens/layout/logoDoHere.png",

        "navItem1" : {
            "rotulo" : "",
            "link" : "",
            "icone" : ""
        },

        "navItem2" : {
            "rotulo" : "Entrar",
            "link" : "/login/",
            "icone" : "log-in"
        }
    }

    def __init__(self, request, abaTitulo="DoHere", paginaTitulo="DOHERE"):
        #Título da Página (aba)
        self.abaTitulo = abaTitulo
        #Título da Página (Nome da página)
        self.paginaTitulo = paginaTitulo
        #Definições do NavBar
        if request.user.is_authenticated:
             self.navbar = {
                "logo" : "imagens/layout/logoDoHere.png",

                "navItem1" : {
                    "rotulo" : request.user.usuario.nome,
                    "link" : "/painel/",
                    "icone" : "user"
                },

                "navItem2" : {
                    "rotulo" : "Sair",
                    "link" : "/logout/",
                    "icone" : "log-out"
                }
            }

    #Setters
    def defPaginaTitulo(self, novoNome):
        self.paginaTitulo = novoNome



#Validação do número de CPF ##########################################################################################
def validar_cpf(cpf):

    cpf = ''.join(re.findall('\d', str(cpf)))

    if (not cpf) or (len(cpf) < 11):
        return False

    # Pega apenas os 9 primeiros dígitos do CPF e gera os 2 dígitos que faltam
    inteiros = map(int, cpf)
    novo = inteiros[:9]

    while len(novo) < 11:
        r = sum([(len(novo)+1-i)*v for i,v in enumerate(novo)]) % 11

        if r > 1:
            f = 11 - r
        else:
            f = 0
        novo.append(f)

    if re.match(r'^1{11}$|^2{11}$|^3{11}$|^4{11}$|^5{11}$|^6{11}$|^7{11}$|^8{11}$|^9{11}$|^0{11}$', cpf) is not None:
        return False

    # Se o número gerado coincidir com o número original, é válido
    if novo == inteiros:
        return True
    return False


#Validação do número de CNPJ ##################################################################################
def validar_cnpj(cnpj):
    cnpj = ''.join(re.findall('\d', str(cnpj)))

    if (not cnpj) or (len(cnpj) < 14):
        return False

    # Pega apenas os 12 primeiros dígitos do CNPJ e gera os 2 dígitos que faltam
    inteiros = map(int, cnpj)
    novo = inteiros[:12]

    prod = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
    while len(novo) < 14:
        r = sum([x*y for (x, y) in zip(novo, prod)]) % 11
        if r > 1:
            f = 11 - r
        else:
            f = 0
        novo.append(f)
        prod.insert(0, 6)

    if re.match(r'^1{14}$|^2{14}$|^3{14}$|^4{14}$|^5{14}$|^6{14}$|^7{14}$|^8{14}$|^9{14}$|^0{14}$', cnpj) is not None:
        return False

    # Se o número gerado coincidir com o número original, é válido
    if novo == inteiros:
        return True
    return False

def validar_telefone(telefone):
    return bool(re.match(r'^\([1-9]{2}\) (?:[2-8]|9[1-9])[0-9]{3}\-[0-9]{4}$', telefone))



#Verificação das senhas informadas no cadastro de usuário ###########################################################
def verif_senha_dupla(senha1, senha2):
    if senha1 == senha2:
        return True
    else:
        return False



def gerarGraficoObj(graficoTipo, datasets, labels, suggestedMax=0):

    if graficoTipo == 'bar':
        return {    
                    'type': graficoTipo,
                    'data': {
                        'labels': labels,
                        'datasets': datasets
                    },
                    'options': {
                        'scales': {
                            'yAxes': [{
                                'ticks': {
                                    'beginAtZero': True,
                                    'suggestedMax': suggestedMax
                                }
                            }]
                        }
                    }
                }

    elif graficoTipo == 'pie':
        return {
                    'type': 'pie',
                    'data': {
                        'datasets': datasets,

                        'labels': labels
                    }
                }