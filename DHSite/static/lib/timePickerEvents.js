jQuery(document).ready(function($) {

	//TimePicker
    $('input.timePicker').timepicker({
        timeFormat: 'HH:mm',
        interval: 30,
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
});