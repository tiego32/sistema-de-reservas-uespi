jQuery(document).ready(function($) {	
	// Carregamento das Notificações
	// Serão verificadas novas notificações a cada 1 minuto
	var ultimaNotifId = 0;
	var tempoAtualiza = 60000;
	function carregNotificacoes() {
	    $.getJSON("/notificacoes/carregamento/", function(notificacoes, status){
	        if (status == "success"){
	            $(notificacoes).each(function(i, notif) {
	                if (notif.id > ultimaNotifId)
	                    $('#notifDescricao').prepend(
	                        $("<li></li>").html(
	                        	"<a href='/notificacoes/link/"+notif.id+notif.link+"'>"
	                        	+notif.descricao
	                        	+"<br>"
	                        	+"<div class='notifHora'><span style='color:#ccc;'>"+notif.dataHora+"</span></div>"
	                        	+"</a>"
	                        )
	                    );

	                //Checando se está na última iteração
	                if (i == notificacoes.length-1){
	                    //id da última notificação
	                    ultimaNotifId = notif.id;
	                }
	            });

	            if (notificacoes.length > 0){
	                $("#notificacaoZero").addClass('hidden');
	                $("#semNotifMsg").addClass('hidden');
	                $("#notificacaoNum").removeClass('hidden').html(notificacoes.length);
	            }
	        }
	    });
	    setTimeout(carregNotificacoes, tempoAtualiza);
	}
	carregNotificacoes();
});