jQuery(document).ready(function($) {
  //------------------------------------------------------------------------

    //Calendário | Página Reservar
    var calendarioR = $('#calendarioReservar');

    //Validação da seleção no front (Após selecionar o período no calendário)
    var isValidEvent = function(start, end){
        var tz = moment.duration("03:00:00");
        return calendarioR.fullCalendar('clientEvents', function (event) {
            event.start.subtract(tz);
            event.end.subtract(tz);
            return ((event.rendering === "background") &&
                   ((start.isAfter(event.start) && start.isBefore(event.end)) ||
                   (end.isAfter(event.start) && end.isBefore(event.end)) ||
                   ((start.isSame(event.start, 'minute') || start.isBefore(event.start)) &&
                   (end.isSame(event.end, 'minute') || end.isAfter(event.end)))));
        }).length > 0;
    };

    calendarioR.fullCalendar({
        defaultView: 'agendaWeek',
        allDaySlot: true,
        eventLimit: true,
        fixedWeekCount: false,
        themeSystem: 'bootstrap3',
        selectable: true,
        selectLongPressDelay: 500,
        selectMinDistance: 2,
        selectHelper: true,
        minTime: "00:00:00",
        maxTime: "24:00:00",
        scrollTime: "08:00:00",
        //slotDuration: "00:10:00",
        //slotLabelInterval: "00:10:00",
        slotLabelFormat: "H:mm",
        nowIndicator: true,
        contentHeight: $(window).height(),
        slotEventOverlap: false,
        navLinks: true,
        firstDay: 1,

        select: function(start, end, allDay) {
            if (isValidEvent(start, end)){
                calendarioR.fullCalendar('unselect');
                alert('Não é possível fazer uma reserva nos dias/horário selecionados.');
            }else if (end - moment.duration("01:00:00") < start){
                calendarioR.fullCalendar('unselect');
                alert('Sua reserva deve ter o período mínimo de 1 hora');
            }else{
                //Exibição da tela para digitar o Título e Descrição do pedido
                $('.popup').css('display', 'inline-block');
                $('#reservaPeriodo').html('INÍCIO: &nbsp;<strong>'
                    + start.format('DD/MMM - HH:mm')
                    + '</strong> &nbsp; | &nbsp; FIM: &nbsp;<strong>'
                    + end.format('DD/MMM - HH:mm</strong>'));

                $('#reservaInicio').attr('value', start.format());
                $('#reservaFim').attr('value', end.format());
                $('#reservaTitulo').focus();
                $('html, body').animate({scrollTop:150}, "slow");

                //Renderização da seleção no calendário
                calendarioR.fullCalendar('renderEvent', {
                    id: 'seuEvento',
                    title: '', //Vazio porque ainda não foi dado um título
                    start: start,
                    end: end
                }, true);
            }
        },

        //Período permitido para criar reserva
        validRange: function(nowDate) {
            return {
                start: nowDate.clone(),
                end: nowDate.clone().add(5, 'months')
            };
        },

        loading: function( isLoading, view ) {
            if(isLoading) {
                $("#telaCheia").css('display', 'block')
            } else {
                $("#telaCheia").css('display', 'none')
            }
        },

        eventRender: function(eventObj, $el) {
            $el.popover({
                title: "",
                content: eventObj.description,
                trigger: 'hover',
                placement: 'top',
                container: 'body'
            });
        },
        
        header: {
            left: 'voltar,today,prev,next month,agendaWeek,agendaDay',
            right: 'title'
        },

        customButtons: {
            voltar: {
                text: 'Voltar',
                click: function() {
                    window.location = "/reservar/pr/selUnidade/";
                }
            }
        },

        views: {
            month:{
                titleFormat: 'MMMM/YYYY'
            },
            week:{
                titleFormat: 'DD/MM/YYYY'
            }
        },

        eventSources: [
            {
                url:'/reservasJson/1' //Reservas Ativas
            },
            {
                url:'/reservasJson/3', //Reservas Em Análise
                color: '#fb6',
                textColor: '#d74'
            },
            {
                url:'/feriadosJson/', //Feriados e dias indisponíveis
                color: '#9d9'
            }
        ]

    });// Calendário Reservar
});



 

