jQuery(document).ready(function($) {
    
    //Calendário | Página Calendário
    var calendario = $('#calendario');
    var campusId   = $('#campus option:selected').val();
    if (typeof campusId === "undefined" || campusId === "Selecione"){
        campusId = '';
    }else{
        campusId = campusId.replace('/campus','').replace('/calendario/','');
    }

    calendario.fullCalendar({
        defaultView: 'agendaWeek',
        allDaySlot: true,
        eventLimit: true,
        themeSystem: 'bootstrap3',
        fixedWeekCount: false,
        nowIndicator: true,
        minTime: "00:00:00",
        maxTime: "24:00:00",
        scrollTime: "08:00:00",
        //slotDuration: "00:10:00",
        //slotLabelInterval: "00:10:00",
        slotLabelFormat: "H:mm",
        contentHeight: $(window).height(),
        slotEventOverlap: false,
        navLinks: true,
        firstDay: 1,

        loading: function( isLoading, view ) {
            if(isLoading) {
                $("#telaCheia").css('display', 'block')
            } else {
                $("#telaCheia").css('display', 'none')
            }
        },

        eventRender: function(eventObj, $el) {
            $el.popover({
                title: "",
                content: eventObj.description,
                trigger: 'hover',
                placement: 'top',
                container: 'body'
            });
        },

        header: {
            left: 'reservar,today,prev,next month,agendaWeek,agendaDay',
            right: 'title'
            //right: 'month,agendaWeek,agendaDay'
        },

        customButtons: {
            reservar: {
                text: 'Criar Reserva',
                click: function() {
                    window.location = "/reservar/_/selModo/";
                }
            },
            voltar: {
                text: 'Voltar',
                click: function() {
                    window.location = "/painel/";
                }
            }
        },

        views: {
            month:{
                titleFormat: 'MM/YYYY'
            },
            week:{
                titleFormat: 'DD/MM/YYYY'
            }
        },

        eventSources: [
            {
                url:'/reservasJson/1/_/'+campusId //Reservas Aceitas
            },
            {
                url:'/feriadosJson/'+campusId, //Dias indisponíveis e Feriados
                color: '#9d9'
            }
        ]
    });// Calendário

});



 

