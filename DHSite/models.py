# -*- coding: utf-8 -*-

from __future__                 import unicode_literals
from django.db                  import models
from django.contrib.auth.models import User
from datetime                   import datetime


#Status dos Pedidos Solicitados pelos Usuários
class pedidoStatus(models.Model):
    class Meta:
        verbose_name        = "Status de Pedidos"
        verbose_name_plural = "Status de Pedidos"

    nome            = models.CharField(max_length=50, unique=True)  #Aceito, Negado, Em análise, Cancelado
    cor             = models.CharField(max_length=10) #Padrão Html Regex: "#[0-9a-fA-F]{6}|[0-9a-fA-F]{3}"
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.nome


#Temas da Plataforma
class painelTema(models.Model):
    class Meta:
        verbose_name        = "Tema do Painel"
        verbose_name_plural = "Temas do Painel"

    titulo          = models.CharField(max_length=20, unique=True) #O primeiro deve ser o padrão
    arquivoCss      = models.CharField(max_length=100) #Endereço do arquivo CSS que define o estilo do tema
    ativo           = models.BooleanField() 
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.titulo


#Usada para guardar os tipos de usuário e seus níveis de acesso
class usuarioTipo(models.Model):
    class Meta:
        verbose_name        = "Tipo de Usuário"
        verbose_name_plural = "Tipos de Usuário"

    nome               = models.CharField(max_length=50, unique=True) #Professor, Aluno, Técnico, Pessoa Física, Pessoa Jurídica
    painelTema         = models.ForeignKey(painelTema) # Estilização CSS para o painel inicial
    permissoes         = models.TextField() #Lista de permissoes específicas para cada tipo de usuário
    #Atributos de cadastro (null=True) da tabela "usuario" (ex: cpf, cnpj...). usados na página "Cadastro de usuário"
    cadastroInputLista = models.CharField(max_length=200)
    #Atributos de dados pessoais (null=True) da tabela "usuario" (ex: cpf, cnpj...). usados na página "Dados Cadastrais"
    dadosInputLista    = models.CharField(max_length=200)
    pedidosMax         = models.PositiveSmallIntegerField() #Quantidade máxima de pedidos ativos
    requerOficio       = models.BooleanField(default=True)
    ativo              = models.BooleanField()
    dataCriacao        = models.DateTimeField(auto_now_add=True)
    dataModificacao    = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.nome


#Tipos de recurso
class recursoTipo(models.Model):
    class Meta:
        verbose_name        = "Tipo de Recurso"
        verbose_name_plural = "Tipos de Recurso"

    nome            = models.CharField(max_length=50, unique=True) #Espaço, Equipamento, Transporte
    ativo           = models.BooleanField()
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.nome


#Guarda os enderecos dos usuário, câmpus e unidades
class endereco(models.Model):
    class Meta:
        verbose_name        = "Endereço"
        verbose_name_plural = "Endereços"

    cep             = models.CharField(max_length=9)
    rua             = models.CharField(max_length=100)
    numero          = models.CharField(max_length=10)
    complem         = models.CharField(max_length=100, null=True, blank=True)
    bairro          = models.CharField(max_length=30, blank=True)
    cidade          = models.CharField(max_length=30)
    uf              = models.CharField(max_length=2)
    ativo           = models.BooleanField() 
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.rua+", "+self.numero+" - "+self.cidade+"-"+self.uf


#Câmpus da UESPI
# O endereço do campus é o endereço da sua unidade sede
class campus(models.Model):
    class Meta:
        verbose_name        = "Campus"
        verbose_name_plural = "Câmpus"

    nome            = models.CharField(max_length=100, unique=True)
    direcao         = models.CharField(max_length=100)
    ativo           = models.BooleanField() 
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.nome


#Unidades pertencentes a cada Campus da UESPI
class unidade(models.Model):
    class Meta:
        verbose_name        = "Unidade"
        verbose_name_plural = "Unidades"

    nome            = models.CharField(max_length=100)
    endereco        = models.ForeignKey(endereco)
    telefone        = models.CharField(max_length=15, blank=True)
    campus          = models.ForeignKey(campus)
    sede            = models.BooleanField()
    ativo           = models.BooleanField()
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.nome


#Cursos da Instituição, usados para alunos e professores
class curso(models.Model):
    class Meta:
        verbose_name        = "Curso"
        verbose_name_plural = "Cursos"

    nome            = models.CharField(max_length=30)
    campus          = models.ForeignKey(campus)
    ativo           = models.BooleanField()
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.nome


#Setores de trabalho na Instituição
class setor(models.Model):
    class Meta:
        verbose_name        = "Setor"
        verbose_name_plural = "Setores"

    nome            = models.CharField(max_length=50)
    campus          = models.ForeignKey(campus)
    ativo           = models.BooleanField()  # $$
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.nome

#Tabela de sexos
class sexo(models.Model):
    class Meta:
        verbose_name        = "Sexo"
        verbose_name_plural = "Sexos"

    nome            = models.CharField(max_length=30, unique=True)
    ativo           = models.BooleanField()
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.nome


#Usuário da plataforma
class usuario(models.Model):
    class Meta:
        verbose_name        = "Usuário"
        verbose_name_plural = "Usuários"

    user            = models.OneToOneField(User) #Relacionando com o "User" padrão do Django
    nome            = models.CharField(max_length=100)
    tipo            = models.ForeignKey(usuarioTipo)
    dataNasc        = models.DateField(null=True)
    cpf             = models.CharField(max_length=11, unique=True, null=True)  
    cnpj            = models.CharField(max_length=14, unique=True, null=True, blank=True)
    sexo            = models.ForeignKey(sexo, null=True)
    endereco        = models.ForeignKey(endereco, null=True)
    telefone        = models.CharField(max_length=15, null=True)
    profissao       = models.CharField(max_length=50, null=True, blank=True)
    matricula       = models.CharField(max_length=8, null=True, unique=True, blank=True)
    curso           = models.ForeignKey(curso, null=True, blank=True)
    setor           = models.ForeignKey(setor, null=True, blank=True)
    campus          = models.ForeignKey(campus)
    permissoes      = models.TextField(null=True, blank=True) #Lista de permissoes específicas para cada usuário
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return self.nome + u' - ' + unicode(self.tipo)


#espaco, equipamento e transporte
class recurso(models.Model):
    class Meta:
        verbose_name        = "Recurso"
        verbose_name_plural = "Recursos"

    nome            = models.CharField(max_length=100, unique=True)
    tipo            = models.ForeignKey(recursoTipo)
    descricao       = models.TextField(max_length=1000)
    fotos           = models.CharField(max_length=1000) #lista de endereços das fotos no servidor $$
    unidade         = models.ForeignKey(unidade) #Unidade do campus a qual pertence o recurso
    quantidade      = models.PositiveIntegerField(default=1)
    disponibUsuario = models.CharField(max_length=100)
    dispHoraInicio  = models.TimeField(default=datetime.strptime('2019-01-01T08:00:00', "%Y-%m-%dT%H:%M:%S").time())
    dispHoraFim     = models.TimeField(default=datetime.strptime('2019-01-01T22:00:00', "%Y-%m-%dT%H:%M:%S").time())
    requerAvaliacao = models.BooleanField(default=True)
    requerOficio    = models.BooleanField(default=True)
    ativo           = models.BooleanField()
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)

    def __unicode__(self):
       return self.nome


#Reservas que serão exibidas no Calendário
class reserva(models.Model):
    class Meta:
        verbose_name        = "Reserva"
        verbose_name_plural = "Reservas"

    titulo          = models.CharField(max_length=100)
    descricao       = models.TextField(max_length=1000)
    inicio          = models.DateTimeField()
    fim             = models.DateTimeField()
    diaInteiro      = models.BooleanField(default=False) #Sinaliza uma reserva de 0h-24h ou período de dias inteiros
    ativo           = models.BooleanField() 
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return unicode(self.titulo)


#Tabela dos Pedidos feitos pelos usuários
# Função para gerar o caminho onde será armazenado no servidor
# def diretorioDoOficio(instance, filename):
#     return 'pedidos/{0}/{1}'.format(instance.id, filename)

class pedido(models.Model):
    class Meta:
        verbose_name        = "Pedido"
        verbose_name_plural = "Pedidos"

    usuario            = models.ForeignKey(usuario)
    recursos           = models.CharField(max_length=120) #Lista de recursos selecionados pelo usuário [recursoId,quantidade]
    reserva            = models.ForeignKey(reserva)
    observacao         = models.TextField(max_length=500, null=True, blank=True) #Motivo de Cancelamento/Negação do pedido
    campusUnidade      = models.ForeignKey(unidade)
    solicitanteNome    = models.CharField(max_length=100, null=True, blank=True)
    solicitanteContato = models.CharField(max_length=30, null=True, blank=True)
    #Endereço na pasta static do ofício de solicitação exigido para os tipos de usuário externos
    oficio             = models.CharField(max_length=50, null=True, blank=True)
    status             = models.ForeignKey(pedidoStatus)
    dataAvaliacao      = models.DateTimeField(null=True)
    expirado           = models.BooleanField(default=False)
    dataCriacao        = models.DateTimeField(auto_now_add=True)
    dataModificacao    = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return unicode(self.reserva) + u" - " + unicode(self.solicitanteNome)


#Feriados e Dias de indisponibilidade dos campi
# Se em um determinado feriado for possível que o campus esteja disponível, então não é necessário criar/registrar um
# Nesse caso, entende-se feriado como um dia em não podem haver reservas de nenhuma forma
class feriado(models.Model):
    class Meta:
        verbose_name        = "Feriado"
        verbose_name_plural = "Feriados"

    data            = models.DateTimeField()
    nome            = models.CharField(max_length=100)
    campus          = models.ForeignKey(campus, null=True) # Se campus for None, o feriado é estadual ou nacional
    ativo           = models.BooleanField()
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return unicode(self.data) + u" - " + unicode(self.nome)


#Opções do painel inicial # $$
class painelOpcao(models.Model):
    class Meta:
        verbose_name        = "Opção do Painel"
        verbose_name_plural = "Opções do Painel"

    titulo           = models.CharField(max_length=100)
    glyphicon        = models.CharField(max_length=20)
    link             = models.CharField(max_length=100)
    precedencia      = models.SmallIntegerField(default=0)
    exibeNotifNumero = models.BooleanField(default=False)
    dataCriacao      = models.DateTimeField(auto_now_add=True)
    dataModificacao  = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return unicode(self.titulo)


#Permissões do sistema para Controle de Acesso dos usuários # $$
class permissao(models.Model):
    class Meta:
        verbose_name        = "Permissão"
        verbose_name_plural = "Permissões"

    descricao        = models.CharField(max_length=100)
    painelOpcao      = models.ForeignKey(painelOpcao, null=True, blank=True)
    ativo            = models.BooleanField()
    dataCriacao      = models.DateTimeField(auto_now_add=True)
    dataModificacao  = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return unicode(self.descricao)


#Notificações de mudança de status dos pedidos para usuários e admins
class notificacao(models.Model):
    class Meta:
        verbose_name        = "Notificação"
        verbose_name_plural = "Notificações"

    descricao        = models.CharField(max_length=100)
    usuarioOrigem    = models.ForeignKey(usuario, related_name='usuarioOrigem', null=True)
    #Notificação enviada diretamente para um usuário específico
    usuarioDestino   = models.ForeignKey(usuario, null=True, related_name='usuarioDestino')
    usuarioExcluido  = models.ForeignKey(usuario, null=True, related_name='usuarioExcluido')
    #Notificação para todos os usuários que têm uma determinada permissão
    permissaoDestino = models.ForeignKey(permissao, null=True, related_name='permissaoDestino')
    link             = models.CharField(max_length=100)
    campus           = models.ForeignKey(campus, null=True)
    visto            = models.BooleanField(default=False)
    vistoPor         = models.TextField()
    dataCriacao      = models.DateTimeField(auto_now_add=True)
    dataModificacao  = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return unicode(self.descricao)


#LOG das atividades no sistema
class log(models.Model):
    class Meta:
        verbose_name        = "Log"
        verbose_name_plural = "Logs"

    atividade       = models.CharField(max_length=500)
    usuario         = models.ForeignKey(usuario)
    dataCriacao     = models.DateTimeField(auto_now_add=True)
    dataModificacao = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return unicode(self.descricao)


# Configurações Gerais do Sistema
class configGeral(models.Model):
    class Meta:
        verbose_name        = "Configuração Geral"
        verbose_name_plural = "Configurações Gerais"

    instituicaoNome      = models.CharField(max_length=50, default="UNIVERSIDADE ESTADUAL DO PIAUÍ")
    instituicaoNomeCurto = models.CharField(max_length=10, default="UESPI")
    reitor               = models.CharField(max_length=50)
    #Início do Período de avaliação de um pedido Em dias
    periodoAvalInicio    = models.PositiveIntegerField()
    #Fim do Período de avaliação de um pedido Em dias
    periodoAvalFim       = models.PositiveIntegerField()
    #Intervalo para atualização das notificações em milisegundos (ms)
    notifAtualizInterv   = models.PositiveIntegerField(default=60000)
    termosDeUso          = models.TextField()
    sistemaAtivo         = models.BooleanField()
    dataCriacao          = models.DateTimeField(auto_now_add=True)
    dataModificacao      = models.DateTimeField(auto_now=True)
    def __unicode__(self):
       return unicode('Registro único! Configuração Geral do Sistema')






################# Autenticação por E-mail #####################
from django.contrib.auth          import get_user_model
from django.contrib.auth.backends import ModelBackend

class EmailBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        try:
            user = UserModel.objects.get(email=username)
        except UserModel.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user
        return None
