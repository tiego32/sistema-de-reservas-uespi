
// Eventos JQUERY

// A MAIOR PARTE DO CÓDIGO ABAIXO NÃO ESTÁ COMENTADA -------------- !!!!!!!!

jQuery(document).ready(function($) {

    //plugin LimitText. Contador de caracteres em campos de texto
    $("#reservaDescricao").limitText({
        limit: 1000,
        warningLimit: 0
    });


    $("#observacao").limitText({
        limit: 500,
        warningLimit: 0
    });

    $("#ajuda2").modal("show");

    //Prevenindo o usuário de continuar o pedido sem selecionar ao menos 1 recurso
    $('#submitSelRecursos').click(function(e){
        if ($('#formSelRecursos input[type=checkbox]:checked').length == 0){
            e.preventDefault();
            alert("Selecione pelo menos 1 recurso!");
        }
    });


    $(".rowSelect").click(function(){
        var inputRadioAtual = $(this).find(".recursoSelecionado");
        var okIconeAtual    = $(this).find('.okIcone');

        $('.recursoSelecionado').prop('checked', false);
        $('.rowSelect').removeAttr("style");
        $('.okIcone').css('display', 'none');

        inputRadioAtual.prop('checked', true);
        okIconeAtual.css('display', 'inline');
        $(this).css("background-color", "#f0f8ff");
    });


    $(".selecaoFixa").click(function(){
        var inputRadioAtual = $(this).parent("div").find('.selecaoFixaOpcao');
        var okIconeAtual    = $(this).find('.okIcone');

        $('.selecaoFixaOpcao').prop('checked', false);
        $('.selecaoFixa').removeAttr("style");
        $('.okIcone').css('display', 'none');

        inputRadioAtual.prop('checked', true);
        okIconeAtual.css('display', 'inline');
        $(this).css("background-color", "#0099df").css("color", "white");
        $('#selecaoFixaForm').attr('action', inputRadioAtual.data("href"));
    });

    $('input[name=usuarioTipo]').click(function(){
        $('#cadastroForm').attr('action', "/usuarioCadastro/formulario/"+ $('input[name=usuarioTipo]:checked').val() +"/");
    });

    $(".rowLink").click(function() {
        window.location = $(this).data("href");
    });

    //Solicitações de reserva | Mudança do form action ao selecionar uma opção de avaliação (Aceitar ou Negar)
    $('.admDecisao').click(function(){
        $('#admDecisaoForm').attr('action', $(this).data("href"));
    });

    //Escolha do período de reserva | Cancelamento de escolha do período de reserva
    $('#cancelar').click(function(){
    	$('.popup').css('display', 'none');
        $('#calendarioReservar').fullCalendar('removeEvents', 'seuEvento');
        $('#calendarioReservarRP').fullCalendar('removeEvents', 'seuEvento');
    });

    $("input[name='quant']").on('keyup keydown', function(e) {
        var max = parseInt($(this).attr('max'));
        if((parseInt($(this).val()) > max)
            && e.keyCode != 46
            && e.keyCode != 8
           ) {
           e.preventDefault();
           $(this).val(max.toString());
        }
        if(parseInt($(this).val()) < 0){
           e.preventDefault();
           $(this).val(0);
        }
    });

    $("input[name='quant']").on({

        //Evitando inputs vazios
        blur: function(e) {
            if($(this).val() == ""){
               $(this).val(0);
            }
        },

        //Sincronização dos inputs 'checkbox' com inputs 'number'
        change: function() {

            //Setando o checkbox
            if($(this).prop('value') > 0) {
                $("input[value='"+ $(this).prop('id') +"']").prop('checked', true);
            } else {
                $("input[value='"+ $(this).prop('id') +"']").prop('checked', false);
            }
        },

        //Permissão negada para dar quebra de linha ao teclar Enter na página de seleção dos reursos
        keypress: function(event) {
            if (event.which == 13) {
                $("input[type='submit']").focus();
                event.preventDefault();
            }
        }
    });

    $("#confirmaReserva").on('click', function(event){
        if (! $("#termosDeUsoAceito").prop('checked')){
            event.preventDefault();
            alert("Por favor, você precisa aceitar os Termos e Condições de Uso!");
        }
        else if ($("#oficioSolicitacaoInput").get(0).files.length === 0){
            event.preventDefault();
            alert("Por favor, anexe o seu Ofício de Solicitação!");
        }
    });

    //Sincronização dos inputs 'checkbox' com inputs 'number'
    $("input[name='recurso']").change(function() {

        if($(this).prop('checked') == true) {
            $("input[id='"+ $(this).prop('value') +"']").prop('value', '1');
        } else {
        	$("input[id='"+ $(this).prop('value') +"']").prop('value', '0');
        }
    });

    // Eventos "collapsible"
    var coll = $("#collapsible-title");
    coll.click(function(e) {
        this.classList.toggle("collapsible-active");
        var content = $("#collapsible-content");
        if (content.css('display') === 'block') {
            coll.html("+ Detalhes")
            content.css('display', 'none');
        } else {
            coll.html("- Detalhes")
            content.css('display', 'block');
        }
    });
});



// Peguei na internet sem medo de ser feliz


//Fotos dos recursos - Altera o atributo "src" da tag "img"
function changeImgSrc(input){
    if (input.files && input.files[0]){
        var reader = new FileReader();
        var rcFoto = $("#"+input.name);

        reader.onload = function(e){
            rcFoto.attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

//Carregamento de um arquivo (Ofício)
function arquivoSel(input){
    if (input.files && input.files[0]){
        if (input.files[0].size > 10485760){
            input.value = "";
            alert("Seu arquivo é maior que 10Mb! Não é possível anexá-lo.");
        }else{
            var reader   = new FileReader();
            var ofcLabel = $("#"+input.name);

            reader.onload = function(e){
                ofcLabel.addClass('btn-default');
                ofcLabel.removeClass('btn-primary');
                ofcLabel.find('#rotulo').html("Arquivo Anexado <span class='glyphicon glyphicon-ok'></span>");
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
}